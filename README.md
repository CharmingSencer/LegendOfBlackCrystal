## Brief Summary
This is an online implementation of a board game called *The Message: the Blacklist (风声·黑名单)* made for fans.


## To Install the Game
Available releases of the client installation packages are listed in the root directory. 

To install the game, download the **latest** release please. 

Note: ***AdobeAir*** is required to be installed on your computer to allow running these installation packages. 

[Download AdobeAir](https://get.adobe.com/air/)


## Source Code Location
The source code of the server is located in the folder *"SmartFoxServerPRO_1.6.6/Server/javaExtensions/extension"*.

The source code of the client is in the folder *"fs_serve"*.