package extension.tasks;

/**
 *一位其他玩家因你的技能宣告胜利时，你没有红蓝情报。
 */
public class Task46 extends TaskBase{
	@Override
	public Boolean check() {
		if(bf.victoryMan!=null && bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).bySantaClaus==true){
			return owner.redCards.size()+owner.blueCards.size()==0;
		}
		return super.check();
	}
}
