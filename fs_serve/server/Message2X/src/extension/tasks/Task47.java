package extension.tasks;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;

/**
 * 一位其他玩家因你的情报死亡时，你的面前有【爆竹】。
 */
public class Task47 extends TaskBase{

	@Override
	public Boolean check() {
//		if(bf.deadman!=null){
		if(bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.getIsDead() && !owner.getIsDead()){
			if(bf.nowGetCards.get(0).getOwner()==owner){
				for(Card c:owner.infocards){
					if(c.getId()==23) return true;
				}
			}
		}
//		}
		return super.check();
	}

}
