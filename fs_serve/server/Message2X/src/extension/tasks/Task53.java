package extension.tasks;

import extension.cons.StepCons;

/**
 *集齐八张手牌，且你面前没有黑情报。
 */
public class Task53 extends TaskBase{
	@Override
	public Boolean check() {
		return (owner.handcards.size()>=8 && owner.blackCards.size()==0);
	}
}
