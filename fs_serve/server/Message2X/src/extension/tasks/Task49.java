package extension.tasks;

import extension.cards.Card;
import extension.cons.StepCons;
import it.gotoandplay.smartfoxserver.SmartFoxServer;

/**
 *亲手让另外一位获得二张或以下红蓝情报的玩家死亡
 */
public class Task49 extends TaskBase{
	@Override
	public Boolean check() {
//		if(bf.deadman!=null){
//		if(bf.willDead==bf.nowGetCardPlayer){
		if(bf.thirdStep==StepCons.BeforeDead && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.getIsDead() && !owner.getIsDead()){
			if(bf.nowGetCards.get(0).getOwner()==owner){
				int d=0;
				for(Card c:bf.nowGetCards){
					if(c.getColor()==4 || c.getColor()==5){
						d++;
					}
				}
				SmartFoxServer.log.info("blue" + bf.nowGetCardPlayer.blueCards.size()+" red"+bf.nowGetCardPlayer.redCards.size()+" d"+d);
				if((bf.nowGetCardPlayer.blueCards.size()+ bf.nowGetCardPlayer.redCards.size()- d)<=2) return true;
			}
		}
		return super.check();
	}
}