package extension.tasks;

import extension.cards.Card;

/**
 *当一位玩家宣胜时，你没有手牌，且本回合内你发动过【授业】。
 */
public class Task44 extends TaskBase{

	@Override
	public Boolean check() {
		if(bf.victoryMan!=null && bf.nowGetCards.size()>0){
			if(owner.handcards.size()==0 && owner.skillhash.get(172).launchCount>0) return true;
		}
		return super.check();
	}

}
