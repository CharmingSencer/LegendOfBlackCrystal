package extension.tasks;

import extension.cards.Card;

/**
 *获得三张或以上的纯蓝情报
 */
public class Task52 extends TaskBase{

	@Override
	public Boolean check() {
		if(!owner.getIsDead()){
			int sb = 0;//solid blue
			for(Card c : owner.blueCards){
				if(c.getColor()==1) ++sb;
			}
			return sb>=3;
		}
		return false;
	}

}
