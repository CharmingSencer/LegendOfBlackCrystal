package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;


/**׷��**/
public class Skill191 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsDead() || !getOwner().getIsOpen())return false;
		if(bf.nowStep==StepCons.InfoReceive && bf.thirdStep==StepCons.AfterDead && bf.skillstep==0 && bf.nowGetCards.size()>0
				&& bf.nowPlayer.getUid()==getOwner().getUid() && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer!=getOwner()){
			for(Card c:bf.nowGetCards){
				if(c.getColor()>2)return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(false);
		playAni(false);
		bf.waitfor(1500);
	}
}
