package extension.skills;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *���Ƴ���
 */
public class Skill201 extends Skill {
	@Override
	public Boolean check() {
		if(!getOwner().getIsOpen() && this.blueSkillCheck() && getOwner().blueCards.size()+getOwner().redCards.size()>=2){
			return true;
		}
		return super.check();
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
		bf.waitfor(1500);
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:tvo.cards){
			temp.add(bf.cardsMap.get(i));
		}
		bf.Burn(getOwner().getUid(), temp);
	}
	
	@Override
	public void excute() {
		if(getOwner().getIsDead()) return;
		ArrayList<Card> cards=new ArrayList<Card>();
		HashSet<Integer> colors = new HashSet<Integer>();
		int i = 0;
		while(true){
			Card c = bf.getCardFromCardPack();
			cards.add(c); //Remove a card from the top of pack and add it into "cards".
			i++;
			if(colors.contains(c.getColor()))break;//If the color set contains this color already, break.
			colors.add(c.getColor());
		}
		
		ActionscriptObject arr1=new ActionscriptObject();
		bf.setCardsResp(arr1, cards);
	
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("rid", getOwner().getRoleId());
//		resp.putNumber("numOfShowed", i);
		resp.putBool("goOn",true);
		resp.put("cards",arr1);
		bf.SendToALL(resp);
		bf.waitfor(1000+(i+1)*1000+1000+1000);
		
		getOwner().addToHand(cards);
		i=0;
	}
	
}
