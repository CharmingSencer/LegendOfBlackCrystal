package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**险中求胜**/
public class Skill185 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen()==false){
//			if((noInforeceive() && selfturn()) || bf.nowStep==StepCons.InfoSend){
			if(blueSkillCheck()){
				for(Player p:bf.roleSeq){
						if(p.infocards.size()<=3)return true;
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
	}

	@Override
	public void excute() {	
		Player target = bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || bf.roleMap.get(getTvo().target).infocards.size()==0){
			return;
		}
		//烧毁目标面前所有情报
		ArrayList<Card> temp=new ArrayList<Card>();
		temp.addAll(target.infocards);
		bf.Burn(target.getUid(), temp);
		//从牌库顶放置等量情报
		ArrayList<Card> temp1=bf.getCardFromCardPack(temp.size());
		temp1.get(0).setOwner(getOwner());
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp1);
		resp.putNumber("from",target.getUid());
		resp.putNumber("type",9);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(target,temp1, 1);
	}
	
}
