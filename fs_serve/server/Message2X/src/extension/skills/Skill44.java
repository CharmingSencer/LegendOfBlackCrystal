package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**收买**/
public class Skill44 extends Skill {

	@Override
	public Boolean check() {
		if(this.blueSkillCheck()){
			if(getOwner().handcards.size() >= (6-getOwner().numberOfInfoColors()) ){
				if(this.hashInfoByOther()) return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("rid", getOwner().getRoleId());
//		resp.putNumber("num",getOwner().handcards.size());
		bf.SendToALL(resp);
		bf.waitfor(1500);
		
//		if(tvo.cards.size()!=6-getOwner().numberOfInfoColors()) return; //Unnecessary, checked on the client side already.
		ArrayList<Card> temp=new ArrayList<Card>();
		for (Integer i : tvo.cards){
			temp.add(getOwner().removeCardFromHand(i, false));
		}
		resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",getTvo().target);
		resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.roleMap.get(getTvo().target).addToHand(temp);
	}

	@Override
	public void excute() {
		if(getOwner().getIsDead() || bf.roleMap.get(getTvo().target).getIsDead() || bf.roleMap.get(getTvo().target).infocards.size()<1){
			return;
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		Player target=bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(target.infocards.size()-1));
			Card card =target.infocards.get(random);
			svo.cards.add(card.getVid());
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ArrayList<Card> temp=new ArrayList<Card>();
		Card car = target.removeCardFromInfo(svo.cards.get(0), false);
		temp.add(car);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",4);//type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.putNumber("from",target.getUid());
		resp.putNumber("to",getOwner().getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(getOwner(), temp, 1);
	}
	
	//红技能版本
//	@Override
//	public Boolean check() {
//		if(bf.usedCardStack.size()==0 && noInforeceive()){
//			if(getOwner().handcards.size()>=4){
//				for(Player p:bf.roleSeq){
//					if(p.getUid()!=getOwner().getUid()){
//						if(p.redCards.size()>0 || p.blackCards.size()>0 || p.blueCards.size()>0)return true;
//					}
//				}
//			}
//		}
//		return false;
//	}
//
//	@Override
//	public void play(SkillVO tvo) {
//		super.play(tvo);
//		playAni(false);
//		
//		bf.sResult.dispose();
//		bf.waitfor(10*1000);
//		userSelected(bf.sResult);
//	}
//
//	private void userSelected(SelectVO svo) {
//		if(svo.cards==null){
//			svo.cards=new ArrayList<Integer>();
//		}
//			if(svo.cards.size()<4){
//				for(Card c:getOwner().handcards){
//					if(svo.cards.indexOf(c.getVid())<0){
//						svo.cards.add(c.getVid());
//						if(svo.cards.size()==4)break;
//					}
//				}
//			}
//			ArrayList<Card> temp=new ArrayList<Card>();
//			for(Integer i:svo.cards){
//				temp.add(getOwner().removeCardFromHand(i, false));
//			}
//			Player target=bf.roleMap.get(getTvo().target);
//			target.addToHand(temp);
//			
//			ActionscriptObject resp=new ActionscriptObject();
//			ActionscriptObject arr=new ActionscriptObject();
//			bf.setCardsResp(arr, temp);
//			resp.putNumber("from",getOwner().getUid());
//			resp.putNumber("to",target.getUid());
//			resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶
//			resp.put("cards",arr);
//			resp.putNumber("h",2);
//			resp.putNumber("f",27);
//			bf.SendToALL(resp);
//			
//			bf.waitfor(2000);
//			temp.clear();
//			Card car=target.removeCardFromInfo(getTvo().card, false);
//			temp.add(car);
//			arr=new ActionscriptObject();
//			bf.setCardsResp(arr, temp);
//			resp.put("cards",arr);
//			resp.putNumber("type",4);
//			resp.putNumber("from",target.getUid());
//			resp.putNumber("to",getOwner().getUid());
//			bf.SendToALL(resp);
//			bf.waitfor(2000);
//			bf.getCard(getOwner(), temp, 1);
//		
//	}
	
}
