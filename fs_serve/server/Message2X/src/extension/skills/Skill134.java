package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;

import java.util.ArrayList;

import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *香风毒雾：当一位不处于沉默状态的其它玩家收到传出的情报时，你可以弃１张手牌，使其进入沉默状态，持续X回合（X为“５”和“场上存活人数”中的较大值）。
 */
public class Skill134 extends Skill {

	@Override
	public Boolean check() {
		if(bf.nowGetCardPlayer!=null && !bf.nowGetCardPlayer.getIsDead() && bf.nowGetCardPlayer!=getOwner() && bf.nowGetCardPlayer.isSilent==0 && getOwner().handcards.size()>0){
			if(bf.nowStep==StepCons.InfoReceive && bf.nowGetCards.size()>0  && bf.skillstep==0){
				return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		tvo.target=this.nowGetCardPlayerUid;
		
		super.play(tvo);
		playAni(false);
		ArrayList<Card> temp=new ArrayList<Card>();
		temp.add(bf.cardsMap.get(tvo.card));
		bf.disCard(getOwner(),temp, 1, null);
		bf.waitfor(1500);
		int num = Math.min(5, bf.getAlivePlayers().size());
		bf.roleMap.get(tvo.target).isSilent=num;
		bf.setSilent(getTvo().target,num);
		bf.waitfor(1000);
	}
}
