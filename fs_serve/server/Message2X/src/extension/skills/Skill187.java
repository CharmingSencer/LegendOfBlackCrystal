package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;


/**同舛之命**/
public class Skill187 extends Skill {
	@Override
	public Boolean check() {
		if(getOwner().skillhash.get(this.id).launchCount<1 && bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).typeOfGetting==0
				&& bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer==getOwner() && !getOwner().getIsDead()){
			for(Card c:bf.nowGetCards){
				if(c.getColor()>2){
					for(Player p : bf.roleSeq){
						if(!p.getIsDead() && p.blackCards.size()<getOwner().blackCards.size()){
//							if(this.hasCardColor(6, getOwner()))
								return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		int diff = getOwner().blackCards.size() - bf.roleMap.get(tvo.target).blackCards.size();
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("diff", diff);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult);
	}
	private void userSelected(SelectVO svo) {
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards!=null){
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject arr=new ActionscriptObject();
			ArrayList<Card> temp=new ArrayList<Card>();
			for(Integer i:svo.cards){
				temp.add(bf.cardsMap.get(i));
			}
			getOwner().removeCardFromHand(temp,false);
			bf.setCardsResp(arr, temp);
			resp.putNumber("from",getOwner().getUid());
			resp.putNumber("to",target.getUid());
			resp.putNumber("type",3);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
			resp.put("cards",arr);
			resp.putNumber("h",2);
			resp.putNumber("f",27);
			bf.SendToALL(resp);
			bf.waitfor(2000);
			bf.getCard(target,temp,1);
		}
		//如果目标是未被沉默的斯特，则不能标记，否则标记
		if(target.getRoleId() == 68 && target.isPoison <= 0){
			SkillVO svo2=new SkillVO();
			svo2.sponsor=target.getUid();
			svo2.target=getOwner().getUid();
			svo2.sid=200; //爱愧交加
			Skill200 s = (Skill200)target.skillhash.get(200);
			s.play2(svo2);
		}else bf.setCommonTarget(target.getUid(), true);
	}
}