package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**潜藏伏击**/
public class Skill9 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen()==false){
//			if((noInforeceive() && selfturn()) || bf.nowStep==StepCons.InfoSend){
			if(blueSkillCheck()){
				if(hashInfoByOther())return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
	}

	@Override
	public void excute() {
//		if(getOwner().getIsDead() || bf.roleMap.get(getTvo().target).getIsDead() || !hasInfoBySomeone(bf.roleMap.get(getTvo().target)) || getTvo().cards.size()==0){ //因为cards在这个技能的客户端里一定会产生，所以这里用size==0判断而不是！=null
//			return;
//		}
//		Card card=bf.cardsMap.get(getTvo().cards.get(0));		
//		Player target=bf.roleMap.get(getTvo().target);
//		if(!target.hasInfoCard(card)){
//			return;
//		}
//		target.removeCardFromInfo(card,false);
//		ActionscriptObject resp=new ActionscriptObject();
//		ActionscriptObject ca=new ActionscriptObject();
//		ActionscriptObject arr=new ActionscriptObject();
//		card.setResponse(ca);
//		arr.put(0,ca);
//		resp.putNumber("from",target.getUid());
//		resp.putNumber("type",6);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶6情报到牌库顶
//		resp.put("cards",arr);
//		resp.putNumber("h",2);
//		resp.putNumber("f",27);
//		bf.SendToALL(resp);
//		bf.cards.add(0, card);
//		bf.updateDeck();
//		bf.waitfor(2000);
		
		if(getOwner().getIsDead() || bf.roleMap.get(getTvo().target).getIsDead() || bf.roleMap.get(getTvo().target).infocards.size()<1){
			return;
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		Player target=bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(target.infocards.size()-1));
			Card card =target.infocards.get(random);
			svo.cards.add(card.getVid());
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ArrayList<Card> temp=new ArrayList<Card>();
		Card car = target.removeCardFromInfo(svo.cards.get(0), false);
		temp.add(car);
//		getOwner().addToHand(car);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",6);//type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.putNumber("from",target.getUid());
//		resp.putNumber("to",getOwner().getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.cards.add(0, car);
		bf.updateDeck();
	}
	
}
