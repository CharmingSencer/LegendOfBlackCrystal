package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *漏洞入侵：你的出牌阶段，若存在已获情报缺少某种颜色的玩家，你可以在其面前放置一张该颜色的情报
 */
public class Skill114 extends Skill {

	//红技能版本
//	@Override
//	public Boolean check() {
//		if(!getOwner().getIsDead() && bf.nowPlayer==getOwner() && getOwner().handcards.size()>0 && (bf.nowStep==StepCons.CardUse1 || bf.nowStep==StepCons.CardUse2) && bf.usedCardStack.size()==0 ){
//			for(Player p:bf.roleSeq){
//				if(p.blueCards.size()==0 || p.redCards.size()==0 ||p.blackCards.size()==0){
//					return true;
//				}
//			}
//		}
//		return false;
//	}
	
//	@Override
//	public void play(SkillVO tvo) {
//		super.play(tvo);
//		playAni(false);
//		bf.sResult.dispose();
//		bf.waitfor(11500);
//		userSelected(bf.sResult);
//		
//	}
//	
//	private void userSelected(SelectVO svo){
//		if(svo.card==0)return;
//		
//		Card card=bf.cardsMap.get(svo.card);
//		getOwner().removeCardFromHand(card,false);
//		ActionscriptObject resp=new ActionscriptObject();
//		ActionscriptObject ca=new ActionscriptObject();
//		ActionscriptObject arr=new ActionscriptObject();
//		card.setResponse(ca);
//		arr.put(0,ca);
//		resp.putNumber("from",getOwner().getUid());
//		resp.putNumber("to",getTvo().target);
//		resp.putNumber("type",3);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶
//		resp.put("cards",arr);
//		resp.putNumber("h",2);
//		resp.putNumber("f",27);
//		bf.SendToALL(resp);
//		
//		bf.waitfor(2000);
//		
//		Player target=bf.roleMap.get(getTvo().target);
//		ArrayList<Card> cards=new ArrayList<Card>();
//		cards.add(card);
//		bf.getCard(target, cards, 1);
//		bf.usedCardStack.removeLast();
//	}
	
	@Override
	public Boolean check() {
		if(!getOwner().getIsDead() && bf.nowPlayer==getOwner() && getOwner().handcards.size()>0 && (bf.nowStep==StepCons.CardUse1 || bf.nowStep==StepCons.CardUse2)
				&& blueSkillCheck() && bf.usedCardStack.size()==0){
//			for (Object o : bf.usedCardStack){
//				if(o instanceof Skill && ((Skill)o).id == 114){ //如果堆叠中有此技能，则返回false
//					return false;
//				}
//			}
			int red=0,blue=0,black=0;
			for(Card c:getOwner().handcards){
				if(c.getColor()==1) blue++;
				if(c.getColor()==2) red++;
				if(c.getColor()==3) black++;
				if(c.getColor()==4) {red++; black++;}
				if(c.getColor()==5) {blue++; black++;}
			}
			for(Player p:bf.roleSeq){
				if( (p.blueCards.size()==0 && blue>0) || (p.redCards.size()==0 && red>0) || (p.blackCards.size()==0 && black>0) ){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		bf.waitfor(1500);
		
	}
	
	@Override
	public void excute() {
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		Player target=bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || getOwner().handcards.size()==0) return;
		if(target.redCards.size()>0 && target.blueCards.size()>0 && target.blackCards.size()>0) return;
//		int red=0,blue=0,black=0;
//		for(Card c:getOwner().handcards){
//			if(c.getColor()==1) blue++;
//			if(c.getColor()==2) red++;
//			if(c.getColor()==3) black++;
//			if(c.getColor()==4) {red++; black++;}
//			if(c.getColor()==5) {blue++; black++;}
//		}
//		if( (target.blueCards.size()==0 && blue>0) || (target.redCards.size()==0 && red>0) || (target.blackCards.size()==0 && black>0) == false) return;
		Card card;
		if(svo.card==0){
			//自动选一张放置
			ArrayList<Card> selectable=new ArrayList<Card>();
			for(Card c : getOwner().handcards){
				switch(c.getColor()){
				case 1:
					if(target.blueCards.size()==0 && selectable.indexOf(c)<0) selectable.add(c);
					break;
				case 2:
					if(target.redCards.size()==0 && selectable.indexOf(c)<0) selectable.add(c);
					break;
				case 3:
					if(target.blackCards.size()==0 && selectable.indexOf(c)<0) selectable.add(c);
					break;
				case 4:
					if( (target.redCards.size()==0 || target.blackCards.size()==0) && selectable.indexOf(c)<0) selectable.add(c);
					break;
				case 5:
					if( (target.blueCards.size()==0 || target.blackCards.size()==0) && selectable.indexOf(c)<0) selectable.add(c);
					break;
				}
			}
			if(selectable.size()==0) return;
			SmartFoxServer.log.info("selectable.size()="+selectable.size());
			int random = (int)(Math.random()*(selectable.size()));
			card = selectable.get(random);
		}else{
			card=bf.cardsMap.get(svo.card);
		}
		SmartFoxServer.log.info("selected card's vid = "+card.getVid());
		getOwner().removeCardFromHand(card,false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject ca=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		card.setResponse(ca);
		arr.put(0,ca);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",getTvo().target);
		resp.putNumber("type",3);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报5手卡到牌库顶
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		
		bf.waitfor(2000);
		
		ArrayList<Card> cards=new ArrayList<Card>();
		cards.add(card);
		bf.getCard(target, cards, 1);
//		bf.usedCardStack.removeLast();
	}
	
}
