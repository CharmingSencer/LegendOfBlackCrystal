package extension.skills;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *情报枢纽
 */
public class Skill208 extends Skill {

	@Override
	public Boolean check() {
		if(this.getExclusiveSkill().launched==0){ //其实可以用auto判断。目前程序中的launched其实是多余的，根本可以不用的。只是留着扩展，万一以后二段技能不是自动了。
			if(!getOwner().getIsDead() && bf.thirdStep==StepCons.InfoSentOut && getOwner().handcards.size()>0){
				return true;
			}	
		}else{
			if(bf.thirdStep==StepCons.RecieveNowInfo && bf.sendTarget.isExtransfer){
				for(Player p : bf.getAlivePlayers(bf.sendTarget)){
					if(p.isExtransfer) return true;
				}
			}
		}
		return false;
	}

	@Override
	public void reset() {
		super.reset();
		this.getExclusiveSkill().launched=0;
		this.getExclusiveSkill().auto=false;
		for(Player p : bf.roleSeq){
			if(p.isExtransfer)bf.setExtransfer(p.getUid(), false);
		}
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		if(this.getExclusiveSkill().launched==0){
			playAni(false);
			bf.waitfor(1500);
			if(tvo.targets==null || tvo.targets.size()!=2) return;
			bf.setExtransfer(tvo.targets.get(0), true);
			bf.setExtransfer(tvo.targets.get(1), true);
			this.getExclusiveSkill().launched=1;
			this.getExclusiveSkill().auto=true;
			ArrayList<Card> temp=new ArrayList<Card>();
			temp.add(bf.cardsMap.get(tvo.card));
			bf.disCard(getOwner(),temp, 1, null);
			bf.waitfor(1500);
		}else{
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject obj=new ActionscriptObject();
			tvo.dur=10000;
			tvo.setResponse(obj);
			resp.put("tvo",obj);
			resp.putNumber("h",2);
			resp.putNumber("f",25);
			resp.putNumber("oid",bf.operId);
			resp.putBool("transfer", true);
			bf.SendToALL(resp);
			bf.waitfor(1500);
			for(Player p : bf.getAlivePlayers(bf.sendTarget)){
				if(p.isExtransfer){
					bf.sendTarget=p;
					break;
				}
			}
			bf.drawCard(getOwner(), bf.getCardFromCardPack(1), 1, null);
			bf.waitfor(1000);
		}
	}
}
