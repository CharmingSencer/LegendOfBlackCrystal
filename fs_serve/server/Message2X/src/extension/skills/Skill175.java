package extension.skills;
import java.util.ArrayList;
import java.util.LinkedList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *未雨绸缪：[响应技]发动：情报传递阶段或你的出牌阶段，翻开此角色牌，指定一位有手牌的其他玩家。结算：检视该玩家的手牌，然后选择１张放回牌库顶。
 */
public class Skill175 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen())return false;
//		if(bf.nowStep==StepCons.InfoSend || (bf.nowPlayer == getOwner() && (bf.nowStep==StepCons.CardUse1 || bf.nowStep==StepCons.CardUse2))){
		if(blueSkillCheck() && bf.nowStep!=StepCons.InfoArrive){
			for (Player p : bf.roleSeq){
				if(p.getUid()!=getOwner().getUid()){
					if(p.handcards.size()>0){
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
	}
	@Override
	public void excute() {
		Player target=bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || target.handcards.size()==0){
//			bf.CardSettlementAnswer();
			return;
		}else{
			LinkedList<Card> temp = target.handcards;
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject obj=new ActionscriptObject();
			ActionscriptObject arr=new ActionscriptObject();
			bf.setCardsResp(arr, temp);
			getTvo().dur=10000;
			getTvo().setResponse(obj);
			resp.put("tvo",obj);
			resp.put("cards",arr);
			resp.putNumber("h",2);
			resp.putNumber("f",25);
			resp.putNumber("oid",bf.operId);
			resp.putBool("goOn", true);
			bf.SendToALL(resp);

			bf.sResult.dispose();
			bf.waitfor(10000);
			useSelected(bf.sResult);
		}
	}

	private void useSelected(SelectVO svo) {
		Player target=bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int num=target.handcards.size();
			num=(int) Math.floor(Math.random()*num);
			svo.cards.add(target.handcards.get(num).getVid());
		}
		
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:svo.cards){
			temp.add(target.removeCardFromHand(i, false));
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",target.getUid());
		resp.putNumber("type",5);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		
		bf.waitfor(2000);
		bf.cards.addAll(0, temp);
		bf.updateDeck();
	}
}
