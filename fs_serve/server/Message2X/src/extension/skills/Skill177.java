package extension.skills;
import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *包裹直达：[响应技]发动：每回合限一次，他人回合的情报传递阶段，翻开此角色牌，并指定一位处于锁定状态或调离状态的其他玩家。结算：将你面前的一张红蓝情报转置到该玩家面前。
 */
public class Skill177 extends Skill {

	@Override
	public Boolean check() {
		if(bf.nowPlayer!=getOwner() && getOwner().skillhash.get(this.id).launchCount<1 && bf.nowStep==StepCons.InfoSend && getOwner().getIsOpen()==false && noSkill()){
			for(Player p : bf.roleSeq){
				if((p.getIsLock() || p.isSkip) && p!=getOwner()){
					return getOwner().blueCards.size()+getOwner().redCards.size()>0;
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
		getOwner().skillhash.get(this.id).launchCount++;
		bf.waitfor(1500);
	}
	
	@Override
	public void excute() {
		Player target = bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead()){
			return;
		}
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo) {
		if(getOwner().infocards.size()==0) return;
		Player target=bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			ArrayList<Integer> index = new ArrayList<Integer>(); //红蓝情报在情报区的index的数组
			int i = 0;
			for (Card c : getOwner().infocards){
				if(c.getColor()!=3){
					index.add(i);
				}
				i ++;
			}
			int num=(int) Math.floor(Math.random()*index.size());
			svo.cards.add(getOwner().infocards.get(index.get(num)).getVid());
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		Card result=getOwner().removeCardFromInfo(svo.cards.get(0), false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		temp.add(result);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",4);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",target.getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(target, temp, 6);
	}
}
