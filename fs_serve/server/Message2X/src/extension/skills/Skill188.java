package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**����֮��**/
public class Skill188 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().skillhash.get(this.id).launchCount<1 && bf.nowStep==StepCons.InfoWait && bf.nowPlayer==getOwner()){
			for(Player p : bf.roleSeq){
				if(p.isCommonTarget==true) return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.waitfor(1500);
		for(Player p : bf.roleSeq){
			if(p.isCommonTarget==true){
				p.setIsLock(true);
			}
		}
	}
}
