package extension.skills;
import java.util.ArrayList;
import java.util.HashSet;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *�����価
 */
public class Skill203 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen() && blueSkillCheck() && (this.hashInfoByOther() || this.hasInfoBySomeone(getOwner()))){
			if(getOwner().handcards.size()>=3){
				int blue=0, red=0, black=0, redblack=0, blueblack=0;
				for(Card c:getOwner().handcards){
					switch(c.getColor()){
					case 1:
						blue++; break;
					case 2:
						red++; break;
					case 3:
						black++; break;
					case 4:
						red++; black++; redblack++; break;
					case 5:
						blue++; black++; blueblack++; break;
					}
				}
				if (red>0 && blue>0 && black>0)
					if ((redblack==0 || red + black - 2 > 0) && (blueblack==0 || blue + black - 2 > 0))
						return true;
			}
		}
		return false;
	}
	
//	@Override
//	public Boolean check() {
//		if(getOwner().getIsOpen() && blueSkillCheck()){
//			if(getOwner().handcards.size()>=3){
//				boolean blue = false, red = false, black = false, idle4=false, idle5=false;
//				for(Card c: getOwner().handcards){
//					if(idle4==true && idle5==true) return true;
//					switch(c.getColor()){
//					case 1:
//						if(red==true && black==true) return true;
//						if(idle5==true){
//							blue=true;
//							black=true;
//						}else blue = true;
//						break;
//					case 2:
//						if(blue==true && black==true) return true;
//						if(idle4==true){
//							red=true;
//							black=true;
//						}else red = true;
//						break;
//					case 3:
//						if(blue==true && red==true) return true;
//						if(idle4==true){
//							red=true;
//							black=true;
////							idle4=false; //unnecessary?
//						}if(idle5==true){
//							blue=true;
//							black=true;
//						}else black = true;
//						break;
//					case 4:
//						if(red==true && (black==true || blue==true)) return true;
//						if(idle4==true){
//							red=true;
//							black=true;
//						}else if (idle5==true){
//							idle4=true;
//						}else if(red==true){
//							black=true;
//						}else if(black==true){
//							red=true;
//						}else{
//							idle4=true;
//						}
//						break;
//					case 5:
//						if(blue==true && (black==true || red==true)) return true;
//						if(idle4==true){
//							idle5=true;
//						}else if (idle5==true){
//							blue=true;
//							black=true;
//						}else if(blue==true){
//							black=true;
//						}else if(black==true){
//							blue=true;
//						}else{
//							idle5=true;
//						}
//						break;
//					}
//					if(blue==true && red==true && black==true) return true;
//				}
//			}
//		}
//		return false;
//	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("rid", getOwner().getRoleId());
		bf.SendToALL(resp);
		bf.waitfor(1500);
		if(tvo.cards.size()!=3)return;
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:tvo.cards){
			temp.add(bf.cardsMap.get(i));
		}
		bf.disCard(getOwner(), temp, 1,null);
		bf.waitfor(2000);
		
		//�Ƿ���ɫ
		resp=new ActionscriptObject();
		obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("turnRole", true);
		bf.SendToALL(resp);
		bf.waitfor(1000);
		getOwner().setIsOpen(false);
	}
	
	@Override
	public void excute() {
		Player target = bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || !this.hasInfoBySomeone(target)){
			return;
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(target.infocards.size()));
			svo.cards.add(target.blackCards.get(random).getVid());
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		Card c=bf.cardsMap.get(svo.cards.get(0));
		temp.add(c);
		bf.Burn(target.getUid(), temp);
		
	}
	
}
