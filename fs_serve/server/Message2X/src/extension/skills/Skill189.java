package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

//���֮��
public class Skill189 extends Skill {

	@Override
	public Boolean check() {
		if(this.getExclusiveSkill().launchCount<1 && !getOwner().getIsDead() && bf.nowGetCards.size()>0 && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.isCommonTarget && bf.thirdStep==StepCons.AfterDead){
			for(Card c:bf.nowGetCards){
				if(c.getColor()>2){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		Player target=this.infosToBeReceived.get(0).getOwner();
		tvo.target=target.getUid();
		playAni(false);
		bf.setCommonTarget(this.nowGetCardPlayerUid, false);
		bf.waitfor(1500);
		bf.drawCard(getOwner(), bf.getCardFromCardPack(1), 1, null);
		bf.waitfor(1000);
		bf.drawCard(target, bf.getCardFromCardPack(1), 1, null);
		bf.waitfor(1000);
		if(this.hasInfoColor(6, getOwner())){
			tvo.target=getOwner().getUid();
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject obj=new ActionscriptObject();
			tvo.dur=10000;
			tvo.setResponse(obj);
			resp.put("tvo",obj);
			resp.putNumber("h",2);
			resp.putNumber("f",25);
			resp.putNumber("oid",bf.operId);
			resp.putBool("chooseBurn", true);
			bf.SendToALL(resp);
			bf.sResult.dispose();
			bf.waitfor(10000);
			userSelected(bf.sResult);
		}
	}
	public void userSelected(SelectVO svo){
		if(svo.cards==null)return;
		ArrayList<Card> temp=new ArrayList<Card>();
		Card c=bf.cardsMap.get(svo.cards.get(0));
		temp.add(c);
		bf.Burn(getOwner().getUid(), temp);
	}
	
}
