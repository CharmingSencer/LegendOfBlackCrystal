package extension.skills;

import extension.cards.ACard;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**־�ڱص�**/
public class Skill169 extends Skill {

	@Override
	public Boolean check() {
		if(bf.nowStep==StepCons.InfoSend && !selfturn() && noSkill() && !bf.sendingcard.security && getOwner().getIsOpen()){
			for(Card card:getOwner().handcards){
				if(card.getSend()==1)return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);

		bf.waitfor(1000);
		Card c=bf.cardsMap.get(tvo.card);
		c.orgColor=c.getColor();
		c.orgId=c.getId();
		c.setColor(c.orgColor);
		c.setId(6);

		TargetVO tvo1=new TargetVO();
		tvo1.sponsor=getOwner().getUid();
		tvo1.cvid=c.getVid();
		bf.usedCardStack.removeLast();
		bf.useCard=tvo1;
		bf.CardLaunch();
	}
	
}
