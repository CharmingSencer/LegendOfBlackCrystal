package extension.skills;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.actions.CardAction11;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;
/**
 *相机行事：[触发技]每当你从牌库抽牌后，你可以执行一次权衡效果。
 */
public class Skill198 extends Skill {
	public CardAction11 c11=null;
	
	@Override
	public Boolean check() {
		if(launchCount<1 && bf.thirdStep==StepCons.AfterDrawFromDeck && bf.drawingPlayer == getOwner() && !this.answer() && !getOwner().getIsDead()) {
			return true;
		}
		return false;
	}
	
	@Override
	public void reset() {
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		getOwner().skillhash.get(this.id).launchCount++;
		bf.waitfor(1500);
		
		if(c11==null){
			c11=new CardAction11();
			c11.setOwner(getOwner());
		}
		TargetVO t=new TargetVO();
		t.useBySkill=true;
		t.sponsor=getOwner().getUid();
		t.cid=11;
		c11.play(t);
		getOwner().skillhash.get(this.id).launchCount--;
	}	
}
