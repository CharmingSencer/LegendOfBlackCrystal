package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**
 * 脱身一击：[触发技]当其他玩家获得来源为你的黑色情报时，你可以翻转此角色牌到“酒保”面。
**/
public class Skill195 extends Skill {

	@Override
	public Boolean check() {
		if(!getOwner().getIsDead() && getOwner().skillhash.get(this.id).launchCount<1 && bf.nowGetCards.size()>0 && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer!=getOwner() && bf.thirdStep==StepCons.AfterDead){
			for(Card c:bf.nowGetCards){
				if(c.getOwner()==getOwner() && c.getColor()>2)return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		bf.SendToALL(resp);
		getOwner().setRoleId(65);
		getOwner().setIndentity(getOwner().getIndentity());
		bf.waitfor(2500);
		
		if(getOwner().task!=null){  //判断酱油任务
			if(getOwner().checkTask()){
				bf.VictoryExcute(getOwner());
				return;
			}
		}
	}
}