package extension.skills;

import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**礼物派送:[触发技]当一位其他玩家成功使用锁定时，你可以抽２张牌，接着交给该玩家和锁定的目标玩家各１张手牌，然后盖伏此角色牌。 **/
public class Skill178 extends Skill {
	@Override
	public Boolean check() {
		if(getOwner().getIsOpen() && getOwner().skillhash.get(this.id).launchCount<1 && bf.thirdStep==0 && bf.subStep==StepCons.CardSettlement && bf.nowSettlement instanceof TargetVO && ((TargetVO)bf.nowSettlement).disabled==false && ((TargetVO)bf.nowSettlement).sponsor!=getOwner().getUid()){
			if(bf.cardsMap.get(((TargetVO)bf.nowSettlement).cvid).getId()==1)return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.waitfor(1000);
		bf.drawCard(getOwner(), bf.getCardFromCardPack(2), 1, null);
		bf.waitfor(1500);
		
		Player from = bf.roleMap.get(((TargetVO)bf.nowSettlement).sponsor);
//		Player to = bf.roleMap.get(((TargetVO)bf.nowSettlement).target);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("from", from.getUid());
//		resp.putNumber("to", to.getUid());
		resp.putNumber("target", from.getUid());
		resp.putBool("handOutToFrom", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10*1000);
		userSelected(bf.sResult);
	}
	
	public void userSelected(SelectVO svo){
		Player from = bf.roleMap.get(((TargetVO)bf.nowSettlement).sponsor);
		Player to = bf.roleMap.get(((TargetVO)bf.nowSettlement).target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int num=getOwner().handcards.size();
			num=(int) Math.floor(Math.random()*num);
			svo.cards.add(getOwner().handcards.get(num).getVid());
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:svo.cards){
			temp.add(getOwner().removeCardFromHand(i, false));
		}
		from.addToHand(temp);
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",from.getUid());
		resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		
		ActionscriptObject resp1=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp1.put("tvo",obj);
		resp1.putNumber("h",2);
		resp1.putNumber("f",25);
		resp1.putNumber("oid",bf.operId);
//		resp1.putNumber("from", from.getUid());
		resp1.putNumber("to", to.getUid());
		resp1.putNumber("target", to.getUid());
		resp1.putBool("handOutToTo", true);
		bf.SendToALL(resp1);
		bf.sResult.dispose();
		bf.waitfor(10*1000);
		userSelected1(bf.sResult);
	}
	
	public void userSelected1(SelectVO svo){
//		Player from = bf.roleMap.get(((TargetVO)bf.nowSettlement).sponsor);
		Player to = bf.roleMap.get(((TargetVO)bf.nowSettlement).target);
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int num=getOwner().handcards.size();
			num=(int) Math.floor(Math.random()*num);
			svo.cards.add(getOwner().handcards.get(num).getVid());
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:svo.cards){
			temp.add(getOwner().removeCardFromHand(i, false));
		}
		to.addToHand(temp);
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",to.getUid());
		resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		
		ActionscriptObject resp1=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp1.put("tvo",obj);
		resp1.putNumber("h",2);
		resp1.putNumber("f",25);
		resp1.putNumber("oid",bf.operId);
//		resp1.putNumber("from", from.getUid());
//		resp1.putNumber("to", to.getUid());
//		resp1.putNumber("target", to.getUid());
		resp1.putBool("turnRole", true);
		bf.SendToALL(resp1);
		bf.waitfor(2000);
		getOwner().setIsOpen(false);
	}
	
}
