package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**粉墨登场**/
public class Skill210 extends Skill {

	@Override
	public Boolean check() {
		if(getExclusiveSkill().launchCount==0 && !getOwner().getIsOpen() && this.blueSkillCheck()){
			return true;
		} else if(getExclusiveSkill().launchCount==1 && bf.subStep==StepCons.CardLaunch && noSkill() && getOwner().handcards.size()>0){
			for(Object t:bf.usedCardStack){
				if(t instanceof TargetVO){
					if(((TargetVO)t).sid==0 && ((TargetVO)t).canDiscover)return true;			
				}
			}
		}
		return false;
	}
	
//	@Override
//	public void reset() {
//		if(getExclusiveSkill().launchCount>0)
//			getExclusiveSkill().launchCount++; //为2
//	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		if(getExclusiveSkill().launchCount==0){ //若为0，为第一段
			playAni(true);
			getOwner().setIsOpen(true);
			bf.waitfor(1000);
		}
		if(getExclusiveSkill().launchCount==1){ //若为1，为第二段
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject obj=new ActionscriptObject();
			tvo.dur=10000;
			tvo.setResponse(obj);
			resp.put("tvo",obj);
			resp.putNumber("h",2);
			resp.putNumber("f",25);
			resp.putNumber("oid",bf.operId);
			resp.putBool("phase2",true);
			bf.SendToALL(resp);
			
			bf.sResult.dispose();
			bf.waitfor(11000);
			userSelected(bf.sResult);
		}
	}
	
	@Override
	public void excute() {
		if(getOwner().getIsDead()){
			return;
		}
		bf.drawCard(getOwner(), bf.getCardFromCardPack(1), 1, null);
		bf.waitfor(1500);
		for(Player p : bf.getSeqPalyers()){
			if(p != getOwner() && p.isSilent==0){
				p.isSilent=1;
				bf.setSilent(p.getUid(), 1);
			}
		}
		bf.waitfor(1000);
		if(getOwner().getIsOpen())getExclusiveSkill().launchCount=1;
	}

	private void userSelected(SelectVO svo) {
		if(svo.card==0){
			int random = (int)(Math.random()*(getOwner().handcards.size()));
			svo.card = getOwner().handcards.get(random).getVid();
		}
		if(svo.target==0){
			for(int i = bf.usedCardStack.size()-1; i>=0; i--){
				if(bf.usedCardStack.get(i) instanceof TargetVO){
					TargetVO tv = (TargetVO) bf.usedCardStack.get(i);
					if(tv.canDiscover=true){
						svo.target = tv.cvid;
						break;
					}
				}
			}
		}
		if(svo.card==0 || svo.target==0) return;
		Card c=bf.cardsMap.get(svo.card);
		c.orgColor=c.getColor();
		c.orgId=c.getId();
		c.setColor(c.orgColor);
		c.setId(4);

		TargetVO tvo1=new TargetVO();
		tvo1.sponsor=getOwner().getUid();
		tvo1.card=svo.target;
		tvo1.cvid=c.getVid();
//		tvo1.canDiscover=false;
		bf.usedCardStack.removeLast();
//		getExclusiveSkill().launchCount--;
		bf.useCard=tvo1;
		bf.CardLaunch();
	}
	
}
