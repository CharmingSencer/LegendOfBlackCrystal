package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.actions.CardAction12;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;


/**黑市交易 [触发技]当其他玩家获得你传出的红蓝情报时，你可以将该玩家面前的一张与该情报含有不同颜色的情报转置到你面前。**/
public class Skill192 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().skillhash.get(this.id).launchCount<1 && bf.nowStep==StepCons.InfoReceive && bf.skillstep==0 && 
				bf.nowGetCards.size()==1 && bf.nowPlayer==getOwner() && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer!=getOwner() && bf.thirdStep==StepCons.AfterDead){
			Card c = bf.nowGetCards.get(0);
			if(c.getColor()!=3){
				int color = c.getColor();
				Player p=bf.nowGetCardPlayer;
				if(color>=1 && color <=2){
					for(Card card:p.infocards){
						if(card.getColor()!=color) return true;
					}
				}else if(color>=4 && color <=5){
					for(Card card:p.infocards){
						if(card.getColor()!=color && card.getColor()!=6-color && card.getColor()!=3) return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		tvo.target=this.nowGetCardPlayerUid;
		Player target = bf.roleMap.get(tvo.target);
		if(getOwner().getIsDead() || target.getIsDead())return;
		super.play(tvo);
		Boolean found = false; //情报区是否有与收到的情报含有不同颜色的情报
		Card c = this.infosToBeReceived.get(0);
		int color = c.getColor();
		ArrayList<Card> availableInfos = new ArrayList<Card>(); //可以转置的情报
		if(color>=1 && color <=2){
			for(Card card:target.infocards){
				if(card.getColor()!=color){
					found = true;
//					if(availableInfos.indexOf(card)<0){
						availableInfos.add(card);
//					}
				}
			}
		}else if(color>=4 && color <=5){
			for(Card card:target.infocards){
				if(card.getColor()!=color && card.getColor()!=6-color && card.getColor()!=3){
					found = true;
//					if(availableInfos.indexOf(card)<0){
						availableInfos.add(card);
//					}
				}
			}
		}
		if(found==false) return; //目标面前已经没有可以转置的请报了
		
		getOwner().skillhash.get(this.id).launchCount++;
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, availableInfos);
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.put("cards", arr);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult, availableInfos);
	}
	
	private void userSelected(SelectVO svo, ArrayList<Card> availableInfos) {
		Player target=bf.roleMap.get(getTvo().target);
		Card card;
		if(svo.cards==null){
			svo.cards = new ArrayList<Integer>();
			int random = (int)(Math.random()*(availableInfos.size()));
			card=availableInfos.get(random);
		}else card=bf.cardsMap.get(svo.cards.get(0));
		card.setOwner(target);
		
		target.removeCardFromInfo(card, false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ArrayList<Card> temp=new ArrayList<Card>();
		temp.add(card);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",4);//type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5手卡到牌库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.putNumber("from",target.getUid());
		resp.putNumber("to",getOwner().getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(getOwner(), temp, 1);
	}
	
}
