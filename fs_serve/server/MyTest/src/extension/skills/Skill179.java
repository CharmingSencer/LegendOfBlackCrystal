package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.ACard;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *猛兽突袭：[触发技]每回合限一次，你的出牌阶段，你可以抽1+X张【爆竹】并交给一位其他玩家，然后随机展示该玩家的1+X张手牌（X为你面前的黑情报数）。将其中的非【爆竹】牌收入你的手牌，【爆竹】移出游戏。然后，你可以在目标玩家面前放置最多Y张情报（Y为移除的【爆竹】数）。最后，目标玩家可以将其手里的任意张【爆竹】放置到你面前。
 */
public class Skill179 extends Skill {

	@Override
	public Boolean check() {
		if(!getOwner().getIsDead() && bf.nowPlayer==getOwner() && getOwner().skillhash.get(this.id).launchCount<1 && (bf.nowStep==StepCons.CardUse1 || bf.nowStep==StepCons.CardUse2) && bf.usedCardStack.size()==0 ){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		int x = getOwner().blackCards.size();
//		ActionscriptObject arr = new ActionscriptObject();
//		ArrayList<Card> temp = new ArrayList<Card>();
//		ACard ac=new ACard();
//		ac.setId(23);
//		ac.setVid(++getOwner().skillhash.get(this.id).fireCracrackerVid);
//		ac.setColor(3);
//		ac.setSend(2);
//		temp.add(ac);
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
//		bf.setCardsResp(arr, temp);
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
//		resp.put("cards",arr);
		resp.putNumber("x", x);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		int fireCrackerNum = 0;
		if(svo.cards==null){ //玩家没选爆竹
			bf.usedCardStack.removeLast();
			return;
		}else{
			fireCrackerNum = svo.cards.size();
		}
		
		ArrayList<Card> fireCrackers = new ArrayList<Card>(); //抽（生成）爆竹
		for(int i = 0; i < fireCrackerNum; ++i){
			ACard ac=new ACard();
			ac.setId(23);
			ac.setVid(++getOwner().skillhash.get(this.id).fireCracrackerVid);
			ac.setColor(3);
			ac.setSend(2);
			bf.cardsMap.put(getOwner().skillhash.get(this.id).fireCracrackerVid, ac);
			fireCrackers.add(ac);	
		}
		
		Player target = bf.roleMap.get(getTvo().target);
		target.addToHand(fireCrackers); //将爆竹交到目标手里
		
		ArrayList<Card> ramPickedHandCards = new ArrayList<Card>(); //随机选择的目标手牌
		for(int i = 0; i < fireCrackerNum; ++i){
			int num=(int) Math.floor(Math.random()*target.handcards.size());
//			ramPickedHandCards.add(target.handcards.get(num));
			ramPickedHandCards.add(target.removeCardFromHand(target.handcards.get(num), false));
		}
//		target.removeCardFromHand(ramPickedHandCards, false);
		
		ArrayList<Card> pickedFireCrackers = new ArrayList<Card>(); //从目标手中抽中的爆竹
		for(Card c : ramPickedHandCards){
			if(c.getId() == 23){
				pickedFireCrackers.add(c);
			}
		}
		
		ArrayList<Card> left = new ArrayList<Card>(); //抽中的其余牌
		for(Card c : ramPickedHandCards){
			if(c.getId() != 23){
				left.add(c);
			}
		}
		
		getOwner().addToHand(left); //获得抽取的非爆竹牌
		bf.sendCardToGraveyard(pickedFireCrackers); //弃置抽取的爆竹
		
		ActionscriptObject arr1 = new ActionscriptObject();
		ActionscriptObject arr2 = new ActionscriptObject();
//		ActionscriptObject arr3 = new ActionscriptObject();
//		ActionscriptObject arr4 = new ActionscriptObject();
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		bf.setCardsResp(arr1, fireCrackers);
		bf.setCardsResp(arr2, ramPickedHandCards);
//		bf.setCardsResp(arr3, pickedFireCrackers);
//		bf.setCardsResp(arr4, left);
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("phase2", true);
		resp.put("fireCrackers",arr1);
		resp.put("ramPickedHandCards", arr2);
//		resp.put("pickedFireCrackers",arr3);
//		resp.put("left", arr4);
		bf.SendToALL(resp);
		
		bf.sResult.dispose();
		bf.waitfor(5000+10*1000); //5 sec of animation
		userSelected1(bf.sResult);
	}
	
	private void userSelected1(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards==null 
//				|| this.hasCardColor(6, getOwner())==false
				)
		{
			//年没选择要放置的黑情报 
			//或 年手里没有黑色手牌
			;
		}else{
			ArrayList<Card> temp=new ArrayList<Card>();
			for(Integer i:svo.cards){
				temp.add(getOwner().removeCardFromHand(i, false));
			}
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject arr=new ActionscriptObject();
			bf.setCardsResp(arr,temp);
			resp.putNumber("from",getOwner().getUid());
			resp.putNumber("to",target.getUid());
			resp.putNumber("type",3);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
			resp.put("cards",arr);
			resp.putNumber("h",2);
			resp.putNumber("f",27);
			bf.SendToALL(resp);
			
			bf.waitfor(2000);
			bf.getCard(target, temp, 1);
		}
		if(!target.getIsDead()){
			ArrayList<Card> firecrackers = new ArrayList<Card>();
			for(Card c: target.handcards){
				if (c.getId() == 23){
					firecrackers.add(c);
				}
			}
			if(firecrackers.size()>0){
				ActionscriptObject arr1 = new ActionscriptObject();
				ActionscriptObject resp1=new ActionscriptObject();
				ActionscriptObject obj=new ActionscriptObject();
				bf.setCardsResp(arr1, firecrackers);
				getTvo().dur=10000;
				getTvo().setResponse(obj);
				resp1.put("tvo",obj);
				resp1.putNumber("h",2);
				resp1.putNumber("f",25);
				resp1.putNumber("oid",bf.operId);
				resp1.putBool("phase3", true);
				resp1.put("firecrackers",arr1);
				bf.SendToALL(resp1);
				
				bf.sResult.dispose();
				bf.waitfor(10*1000); 
				userSelected2(bf.sResult);
			}else{
				bf.usedCardStack.removeLast();
			}
		}else{
			bf.usedCardStack.removeLast();
		}
	}
	
	private void userSelected2(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards==null || this.hasCardColor(6, target)==false){
			//目标玩家没选择要放置的黑情报 或 目标玩家手里没有黑色手牌
			;
		}else{
			ArrayList<Card> temp=new ArrayList<Card>();
			for(Integer i:svo.cards){
				temp.add(target.removeCardFromHand(i, false));
			}
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject arr=new ActionscriptObject();
			bf.setCardsResp(arr,temp);
			resp.putNumber("from",target.getUid());
			resp.putNumber("to",getOwner().getUid());
			resp.putNumber("type",3);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
			resp.put("cards",arr);
			resp.putNumber("h",2);
			resp.putNumber("f",27);
			bf.SendToALL(resp);
			
			bf.waitfor(2000);
			bf.getCard(getOwner(), temp, 1);
		}
		bf.usedCardStack.removeLast();
	}
	
}
