package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**����֮��**/
public class Skill211 extends Skill {

	@Override
	public Boolean check() {
		if( bf.nowPlayer==getOwner() && getOwner().skillhash.get(this.id).launchCount<1 && getOwner().getIsOpen() && bf.nowStep==StepCons.InfoWait && hasCardColor(6,getOwner())){
			for(Player p : bf.roleSeq){
				if(p.isSilent>0) return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.waitfor(1500);
		ChooseInfo();
	}
	public void ChooseInfo(){
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn",true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10*1000);
		ChooseInfoResult(bf.sResult);
	}
	public void ChooseInfoResult(SelectVO svo){
		if(svo.card==0 || svo.target==0){
			if(svo.card==0){
				ArrayList<Card> avai = new ArrayList<Card>();
				for(Card c : getOwner().handcards){
					if(c.getColor()>=3){
						avai.add(c);
					}
				}
				int random = (int)(Math.random()*(avai.size()));
				svo.card = avai.get(random).getVid();
			}
			if(svo.target==0){
				for(Player p : bf.roleSeq){
					if(p.isSilent>0){
						svo.target = p.getUid();
						break;
					}
				}
			}
		}
		if(svo.card==0 || svo.target==0) return;
		Card c=bf.cardsMap.get(svo.card);
		c.orgSend=c.getSend();
		c.setSend(1);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putBool("goOn",true);
		resp.putNumber("uid",svo.target);
		bf.SendToALL(resp);
		bf.roleMap.get(svo.target).setIsLock(true);
	}
	
}
