package extension.skills;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *倒影：[触发技]当你获得其他玩家传出的红蓝情报时，可以在该玩家面前放置一张含有相同颜色的情报。
 */
public class Skill183 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsDead() || !getOwner().getIsOpen())return false;
		if(bf.nowStep==StepCons.InfoReceive && bf.skillstep==0 && bf.nowGetCardPlayer==getOwner() && bf.nowPlayer!=getOwner()){
			for(Card c:bf.nowGetCards){
				if(c.getColor()!=3){
					int color = bf.nowGetCards.get(0).getColor();
					ArrayList<Integer> sameColor=new ArrayList<Integer>();
					switch(color){
					case 1://蓝
						sameColor.add(1);
						sameColor.add(5);
						break;
					case 2://红
						sameColor.add(2);
						sameColor.add(4);
						break;
					case 3://黑
						sameColor.add(3);
						sameColor.add(4);
						sameColor.add(5);
						break;
					case 4://红黑
						sameColor.add(2);
						sameColor.add(3);
						sameColor.add(4);
						sameColor.add(5);
						break;
					case 5://蓝黑
						sameColor.add(1);
						sameColor.add(3);
						sameColor.add(4);
						sameColor.add(5);
						break;
					}
					for(int i = 0; i < getOwner().handcards.size(); i++){
						for(int co : sameColor){
							if(getOwner().handcards.get(i).getColor() == co){
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		tvo.target=bf.nowPlayer.getUid(); //有bug,应该用this的数据
		int color = bf.nowGetCards.get(0).getColor();//有bug,应该用this的数据
		ArrayList<Integer> sameColor=new ArrayList<Integer>();
		switch(color){
		case 1://蓝
			sameColor.add(1);
			sameColor.add(5);
			break;
		case 2://红
			sameColor.add(2);
			sameColor.add(4);
			break;
		case 3://黑
			sameColor.add(3);
			sameColor.add(4);
			sameColor.add(5);
			break;
		case 4://红黑
			sameColor.add(2);
			sameColor.add(3);
			sameColor.add(4);
			sameColor.add(5);
			break;
		case 5://蓝黑
			sameColor.add(1);
			sameColor.add(3);
			sameColor.add(4);
			sameColor.add(5);
			break;
		}
		ArrayList<Card> cards=new ArrayList<Card>();//与所收到情报含有相同颜色的手牌，即可以放置的牌。
		for(int i = 0; i < getOwner().handcards.size(); i++){//构筑cards
			for(int co : sameColor){
				if(getOwner().handcards.get(i).getColor() == co){
					cards.add(getOwner().handcards.get(i));
					break;
				}
			}
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, cards);
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
//		resp.putNumber("color", color);
		resp.put("cards",arr);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult, cards);
	}
		
	private void userSelected(SelectVO svo, ArrayList<Card> cards){
		Player target=bf.roleMap.get(getTvo().target);
		//If the user did not select a target message, pick a random one for him.
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(cards.size()-1));
			Card card =cards.get(random);
			svo.cards.add(card.getVid());
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		Card c=bf.cardsMap.get(svo.cards.get(0));
		temp.add(c);
		getOwner().removeCardFromHand(c,false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",getTvo().target);
		resp.putNumber("type",3); //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(target,temp,1);
	}
	
}
