package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**
 * 高屋建瓴
**/
public class Skill186 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().idenShow || !getOwner().getIsOpen())return false;
		if(selfturn() && this.blueSkillCheck()){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().idenShow=true;
		playAni(false);
		bf.waitfor(1500);
		ActionscriptObject resp=new ActionscriptObject();
		resp.putNumber("from",-1);
		resp.putNumber("target",getOwner().getUid());
		resp.putNumber("iden",getOwner().getIndentity());
		resp.putNumber("h",2);
		resp.putNumber("f",30);
		bf.SendToALL(resp);
		bf.waitfor(3000);
	}
	
	@Override
	public void excute() {
		if(getOwner().getIsDead()) return;
		
		ArrayList<Card> temp = new ArrayList<Card>();  //Cards from deck to show
		int bluec = 0;
		int redc = 0;
		int blackc = 0;
		int mostc = -1;
		for(int i = 1; i <=5; ++i){
			Card c = bf.getCardFromCardPack();
			temp.add(c);
			switch(c.getColor()){
			case 1:
				bluec++;
				break;
			case 2:
				redc++;
				break;
			case 3:
				blackc++;
				break;
			case 4:
				redc++;
				blackc++;
				break;
			case 5:
				bluec++;
				blackc++;
				break;
			}
		}
		mostc = Math.max(blackc, Math.max(bluec,redc)); //出现最多的次数
//		SmartFoxServer.log.info("mostc=" + mostc);
//		SmartFoxServer.log.info("bluec=" + bluec);
//		SmartFoxServer.log.info("redc=" + redc);
//		SmartFoxServer.log.info("blackc=" + blackc);
		ArrayList<Integer> availableColors = new ArrayList<Integer>(); //出现最多的颜色列表（计入双色）
		if(bluec == mostc){
			availableColors.add(1);
			availableColors.add(5);
		}
		if(redc == mostc){
			availableColors.add(2);
			availableColors.add(4);
		}
		if(blackc == mostc){
			availableColors.add(3);
			availableColors.add(4); //有可能插入两次4，不过不影响
			availableColors.add(5); //有可能插入两次5，不过不影响
		}
		ArrayList<Card> temp1=new ArrayList<Card>(); //可以选择放置的牌
		for(Card c : temp){
			for(int co : availableColors){
//				SmartFoxServer.log.info("c.color : co = " + c.getColor() + co );
				if (c.getColor()==co){
					temp1.add(c);
					break;
				}
			}
		}
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ActionscriptObject arr2=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		bf.setCardsResp(arr2, temp1);
		getTvo().dur=15000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.put("cards", arr); //展示牌
		resp.put("cards2", arr2); //可选牌
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(3000+getTvo().dur);
		userSelected(bf.sResult, temp);
	}
	
	private void userSelected(SelectVO svo, ArrayList<Card> cards){
		//若没选目标，则目标是右手边的玩家
		if(svo.target==0){
			svo.target=bf.getNextPlayer(getOwner()).getUid();
		}
		//若没选牌，则随机选一张
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(5-1));
			Card card = cards.get(random);
			svo.cards.add(card.getVid());
		}
		getTvo().target=svo.target;
		Player target= bf.roleMap.get(svo.target);
		ArrayList<Card> originalCards = new ArrayList<Card>();
		originalCards.addAll(cards);
		ArrayList<Card> temp = new ArrayList<Card>();
		Card card = bf.cardsMap.get(svo.cards.get(0));
		card.setOwner(getOwner());
		temp.add(cards.remove(cards.indexOf(card)));
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ActionscriptObject arr1=new ActionscriptObject();
		bf.setCardsResp(arr, originalCards);
		bf.setCardsResp(arr1, temp);
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.put("cards", arr); //展示牌
		resp.put("cards2", arr1); //选的牌
		resp.putBool("goOn2", true);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		
		//先将展示牌放回牌库顶，然后再放置情报
		for(int i = 3; i >= 0; --i){
			bf.cards.add(0, cards.get(i)); //将其余4张牌按顺序放回牌库顶
		}
		bf.updateDeck();
		bf.getCard(target, temp, 1);
	}
	
}
