package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;
/**
 *凛然 当你死亡时，你可以令凶手进入沉默状态或弃掉凶手的所有手牌。
 */
public class Skill173 extends Skill {
	
	@Override
	public Boolean check() {
		if(getOwner().getIsDead()==true && bf.nowGetCards != null && bf.nowGetCardPlayer != null && bf.nowGetCardPlayer.getUid()==getOwner().getUid() && bf.nowGetCards.get(0).getOwner().getUid() != getOwner().getUid()){
			if(bf.deadman!=null && getOwner().skillhash.get(this.id).launchCount<1 && bf.thirdStep==StepCons.AfterDead) return true;
		}
			return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
//		Player target=bf.nowGetCards.get(0).getOwner();
//		SmartFoxServer.log.info("yishuhua play"+this.infosToBeReceived.size());
		Player target=bf.roleMap.get(bf.murderer);
		tvo.target=target.getUid();
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("murderer", tvo.target);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult);
		
	}
	
	private void userSelected(SelectVO svo){//type:1沉默 2弃牌 3取消
		if(svo.type==0)svo.type=3;
		Player target=bf.roleMap.get(getTvo().target);
		switch(svo.type){
		case 1:
			target.isSilent=99;
			bf.setSilent(getTvo().target, 99);
			bf.waitfor(1000);
			break;
		case 2:
			LinkedList<Card> temp=new LinkedList<Card>();
			temp.addAll(target.handcards);
			bf.disCard(target,temp, 1, null);
			bf.waitfor(2000);
			break;
		case 3:
			return;
		}
	}
	
}
