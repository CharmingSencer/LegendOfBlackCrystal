package extension.skills;

import java.util.ArrayList;
import java.util.LinkedList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**���һ��**/
public class Skill212 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen() && bf.thirdStep==0 && bf.subStep==StepCons.CardSettlement
				&& bf.nowSettlement instanceof TargetVO
				&& ((TargetVO)bf.nowSettlement).disabled==false
				&& ((TargetVO)bf.nowSettlement).sponsor==getOwner().getUid()){
			int cindex=bf.cardsMap.get(((TargetVO)bf.nowSettlement).cvid).getId();
			if(cindex>=13 && cindex<=18 && bf.roleMap.get(((TargetVO)bf.nowSettlement).target).isSilent==0)return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		tvo.target=((TargetVO)bf.nowSettlement).target;
		playAni(false);
		bf.waitfor(1500);
		Player target = bf.roleMap.get(tvo.target);
		target.isSilent=1;
		bf.setSilent(tvo.target, 1);
		bf.waitfor(1000);
	}
}
