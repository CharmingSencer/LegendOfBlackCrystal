package extension.skills;
import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *重圆：[响应技]发动：每回合限一次，你的出牌阶段，指定一位其他玩家。结算：你与该玩家同时展示１张手牌。若两张牌含有相同颜色，则你盖伏此角色牌，并可以烧毁你面前的一张黑情报。
 */
public class Skill184 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsOpen() && !getOwner().getIsDead() && getOwner().skillhash.get(this.id).launchCount<1 && selfturn() && (bf.nowStep==StepCons.CardUse1  || bf.nowStep==StepCons.CardUse2) && noInforeceive()){
			if(getOwner().handcards.size()>0){
				for(Player p:bf.roleSeq){
					if(p.getUid()!=getOwner().getUid()){
						if(p.handcards.size()>0)return true;
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public void play(SkillVO tvo) {
		getExclusiveSkill().launchCount++;
		super.play(tvo);
		playAni(true);
		bf.waitfor(1500);
	}
	
	@Override
	public void excute() {
		Player target = bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || getOwner().handcards.size()==0 || target.handcards.size()==0){
			return;
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000); //Give one extra sec if it is required to guarantee that we receive two selected cards' data
		userSelected(bf.sResult);
	}
	
	ArrayList<Card> temp1=new ArrayList<Card>();//你展示的牌
	ArrayList<Card> temp2=new ArrayList<Card>();//目标展示的牌
	private void userSelected(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		Card card = null;
//		Card card2 = null;
		if(svo.card<=0){//如果你没有选要展示的手牌
			int random = (int)(Math.random()*(getOwner().handcards.size()-1));
			card =getOwner().handcards.get(random);
		}else card = bf.cardsMap.get(svo.card);
		temp1.add(card);
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn2", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000); //Give one extra sec if it is required to guarantee that we receive two selected cards' data
		userSelected1(bf.sResult);
	}
	
	private void userSelected1(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		Card card2 = null;
		if(svo.card<=0){//If the target did not choose which card in hand to show 如果目标玩家没有选要展示的手牌
			int random = (int)(Math.random()*(target.handcards.size()-1));
			card2 =target.handcards.get(random);
		}else card2 = bf.cardsMap.get(svo.card);
		temp2.add(card2);
		
		int color1 = temp1.get(0).getColor();
		int color2 = card2.getColor();
		boolean haveSameColor = false;
		switch(color1){
		case 1://蓝
			if(color2 == 1 || color2 == 5) haveSameColor = true;
			break;
		case 2://红
			if(color2 == 2 || color2 == 4) haveSameColor = true;
			break;
		case 3://黑
			if(color2 == 3 || color2 == 4 || color2 == 5) haveSameColor = true;
			break;
		case 4://红黑
			if(color2 == 2 || color2 == 3 || color2 == 4 || color2 == 5) haveSameColor = true;
			break;
		case 5://蓝黑
			if(color2 == 1 || color2 == 3 || color2 == 4 || color2 == 5) haveSameColor = true;
			break;
		}		
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr1=new ActionscriptObject();
		ActionscriptObject arr2=new ActionscriptObject();
		getTvo().setResponse(obj);
		bf.setCardsResp(arr1, temp1);
		bf.setCardsResp(arr2, temp2);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn3",true);
		resp.put("card1", arr1);
		resp.put("card2", arr2);
		resp.putBool("haveSameColor", haveSameColor);
		bf.SendToALL(resp);
		if(haveSameColor){
			getOwner().setIsOpen(false);
			if(getOwner().blackCards.size()==0){
				bf.waitfor(1500);
				return;
			}
			bf.sResult.dispose();
			bf.waitfor(16500);
			userSelected2(bf.sResult);
		}
	}
	
	public void userSelected2(SelectVO svo){
		if(svo.cards==null)return;
		ArrayList<Card> temp=new ArrayList<Card>();
		Card c=bf.cardsMap.get(svo.cards.get(0));
		temp.add(c);
		bf.Burn(getOwner().getUid(), temp);
	}
	
}
