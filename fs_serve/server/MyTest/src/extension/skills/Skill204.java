package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import javax.security.auth.callback.LanguageCallback;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *密令
 */
public class Skill204 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().skillhash.get(this.id).launchCount<1 && bf.nowStep==StepCons.InfoWait && bf.nowPlayer!=getOwner()){
			return getOwner().handcards.size()>0;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.waitfor(1000);
		
		tvo.target=bf.nowPlayer.getUid();
		int cardToSend = tvo.card;
		bf.sResult.card=cardToSend;
//		int targetPlayer = tvo.target;
		Player target=bf.roleMap.get(tvo.target);
		Card card=bf.cardsMap.get(tvo.card);
		ArrayList<Card> temp=new ArrayList<Card>();
		temp.add(card);
		getOwner().removeCardFromHand(card,false);
		target.addToHand(card);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",target.getUid());
		resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2500);
		
		//如果是直达，则等待目标选择直达的目标。
		if(card.getSend()==1){
			resp=new ActionscriptObject();
			ActionscriptObject obj=new ActionscriptObject();
			tvo.dur=10000;
			tvo.setResponse(obj);
			resp.put("tvo",obj);
			resp.putNumber("h",2);
			resp.putNumber("f",25);
			resp.putNumber("oid",bf.operId);
			resp.putBool("goOn", true);
			bf.SendToALL(resp);
			bf.sResult.dispose();
			bf.waitfor(10*1000);
			userSelected(bf.sResult, cardToSend);
		}
	}
	public void userSelected(SelectVO svo, int cardToSend){
		if(svo.target==0){
			int random = (int)(Math.random()*(bf.getAlivePlayers(bf.nowPlayer).size()));
			svo.target = bf.getAlivePlayers(bf.nowPlayer).get(random).getUid();
		}
		bf.sResult.card=cardToSend;
//		SmartFoxServer.log.info("bf.sR.card="+bf.sResult.card);
//		bf.sResult.target=svo.target; //好像不用写
	}
}
