package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;

/**
 * 绝渡逢舟 [响应技]发动：翻开你的身份牌。结算：抽X+2张牌（X为你面前的黑情报数）。
**/
public class Skill199 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().idenShow)return false;
		if(this.blueSkillCheck()){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().idenShow=true;
		playAni(false);
		bf.waitfor(1500);
		ActionscriptObject resp=new ActionscriptObject();
		resp.putNumber("from",-1);
		resp.putNumber("target",getOwner().getUid());
		resp.putNumber("iden",getOwner().getIndentity());
		resp.putNumber("h",2);
		resp.putNumber("f",30);
		bf.SendToALL(resp);
		bf.waitfor(3000);
	}
	
	@Override
	public void excute() {
		if(getOwner().getIsDead()) return;
		bf.drawCard(getOwner(), bf.getCardFromCardPack(1+getOwner().blackCards.size()), 1, null);
		bf.waitfor(1500);
	}
}
