package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.cons.StepCons;
import extension.vo.SkillVO;

/**���ҷ�Դ**/
public class Skill197 extends Skill {
	public Skill197(){
		auto=true;
	}
	@Override
	public Boolean check() {
		if(selfturn() && bf.thirdStep==0 && (bf.nowStep==StepCons.StepBegin || bf.nowStep==StepCons.StepEnd )){
			if(bf.nowStep==StepCons.StepBegin){
				return (getOwner().handcards.size() < 2+getOwner().numberOfInfoColors());
			}else{
				return (getOwner().handcards.size() < Math.min(5, bf.getAlivePlayers().size()-1));
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		bf.waitfor(1000);
		if(bf.nowStep==StepCons.StepBegin){
			bf.drawCard(getOwner(), bf.getCardFromCardPack(2+getOwner().numberOfInfoColors()-getOwner().handcards.size()), 1, null);
		}else{
			int num = Math.min(5, bf.getAlivePlayers().size()-1);
			bf.drawCard(getOwner(), bf.getCardFromCardPack(num-getOwner().handcards.size()), 1, null);
		}
		bf.waitfor(1000);
	}
}
