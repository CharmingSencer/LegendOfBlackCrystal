package extension.skills;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *殊死一搏
 */
public class Skill209 extends Skill {

	@Override
	public Boolean check() {
//		if(getOwner().idenShow)return false;
		if(this.blueSkillCheck() && getOwner().blackCards.size()==2 && this.getExclusiveSkill().launchCount<1){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
//		getOwner().idenShow=true;
		this.getExclusiveSkill().launchCount++;
		playAni(false);
		bf.waitfor(1500);
//		ActionscriptObject resp=new ActionscriptObject();
//		resp.putNumber("from",-1);
//		resp.putNumber("target",getOwner().getUid());
//		resp.putNumber("iden",getOwner().getIndentity());
//		resp.putNumber("h",2);
//		resp.putNumber("f",30);
//		bf.SendToALL(resp);
//		bf.waitfor(3000);
	}
	
	@Override
	public void excute(){
		if(getOwner().getIsDead()){
			return;
		}
		
		ArrayList<Card> temp=bf.getCardFromCardPack(1);
		temp.get(0).setOwner(getOwner());
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("type",9);  //type  1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5手卡到牌库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(getOwner(),temp, 1);
		
		if(getOwner().getIsDead()){
			return;
		}
		Player target = bf.roleMap.get(getTvo().target);
		temp=bf.getCardFromCardPack(1);
		temp.get(0).setOwner(getOwner());
		resp=new ActionscriptObject();
		arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",target.getUid());
		resp.putNumber("type",9);  //type  1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5手卡到牌库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(target,temp, 1);
	}
}
