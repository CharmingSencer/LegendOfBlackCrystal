package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.LinkedList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**
 *爱愧交加
 */
public class Skill200 extends Skill {

	@Override
	public Boolean check() {
		return (bf.thirdStep==StepCons.AfterDead && getOwner().getIsDead()==true && getOwner().skillhash.get(this.id).launchCount<1 && getOwner().handcards.size()>0 && hasDaiSha());
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.waitfor(1000);
		giveOut();
	}
	
	public void play2(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		bf.waitfor(1000);
	}

	private void giveOut() {
		Player target = bf.roleMap.get(getTvo().target);
		ArrayList<Card> temp = new ArrayList<Card>();
		for(Card c : getOwner().handcards){
			temp.add(c);
		}
		getOwner().handcards.clear();
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",getTvo().target);
		resp.putNumber("type",1);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		
		bf.waitfor(2000);
		target.addToHand(temp);
	}
	
	public boolean hasDaiSha(){
		for(Player p:bf.roleSeq){
			if(p.getRoleId()==63 && !p.getIsDead() && !p.isLost) return true;
		}
		return false;
	}
	
}
