package extension.skills;
import java.util.ArrayList;
import java.util.LinkedList;

import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
/**
 *��������
 */
public class Skill93 extends Skill {
	public Skill93(){
		auto=true;
	}
	@Override
	public Boolean check() {
		if(getOwner().getIsDead())return false;
		if(getOwner().getIsOpen() && bf.deadman!=null && bf.thirdStep==StepCons.AfterDead) return true;
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		tvo.sponsor=getOwner().getUid();
		getOwner().setIsOpen(false);
		playAni(false);
		bf.sResult.dispose();
		bf.waitfor(12000);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo) {
		if(svo.target==0) return;
		getTvo().target=svo.target;
		LinkedList<Card> temp=bf.roleMap.get(getTvo().target).handcards;
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000);
		userSelected1(bf.sResult);
	}
	
	private void userSelected1(SelectVO svo) {
		return;
	}
}
