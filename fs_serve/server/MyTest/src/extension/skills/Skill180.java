package extension.skills;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *横行霸道：[被动技]你的回合结束阶段，将你面前的非【爆竹】黑情报收入你的手牌。
 */
public class Skill180 extends Skill {
	public Skill180(){
		auto=true;
	}
	@Override
	public Boolean check() {
		return ( bf.nowStep==StepCons.StepEnd && getOwner().skillhash.get(this.id).launchCount<1 && bf.nowPlayer==getOwner() );
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		ArrayList<Card> temp = new ArrayList<Card>();
		for(Card c : getOwner().infocards){
			if(c.getId() != 23 && c.getColor() >= 3) //若非爆竹且是黑情报
				temp.add(c);
		}
		if(temp.size()>0){
			bf.Burn(getOwner().getUid(), temp);
			bf.waitfor(1500);
		}
	}
}
