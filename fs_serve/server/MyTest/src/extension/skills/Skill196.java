package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *���ܹ���
 */
public class Skill196 extends Skill {

	public Skill196(){
		auto=true;
	}
	@Override
	public Boolean check() {
		if(bf.nowPlayer.getIsDead()==false && bf.nowPlayer.isLost==false && bf.nowPlayer==getOwner() && bf.nowStep==StepCons.DealingSingle && bf.thirdStep==0){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		if(!this.check()) return;
		super.play(tvo);
		getOwner().isSkipDealing=true;
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		SkillVO svo=new SkillVO();
		svo.sponsor=getOwner().getUid();
		svo.sid=id;
		svo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		bf.SendToALL(resp);
		getOwner().skillhash.get(this.id).launchCount++;
		bf.waitfor(1000);
	}
}
