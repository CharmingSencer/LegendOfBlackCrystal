package extension.skills;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;

import extension.Player;
import extension.cards.Card;
import extension.vo.SkillVO;

/**掩护**/

public class Skill17 extends Skill {
//	public Skill17(){
//		auto=true;
//	}
//	@Override
//	public Boolean check() {
//		if(getOwner().getIsOpen()==true && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer==getOwner()){
//			return true;
//		}
//		return false;
//	}
//
//	@Override
//	public void play(SkillVO tvo) {
//		super.play(tvo);
//		getOwner().setIsOpen(false);
//		playAni(false);
//		bf.waitfor(1500);
//	}
	
	@Override
	public Boolean check() {
		if(getOwner().getIsOpen() && !getOwner().getIsDead() && blueSkillCheck()){
			for(Player p: bf.roleSeq){
				if(p!=getOwner()){
					if(getOwner().redCards.size()==2 && getOwner().blueCards.size()==2){
						for(Card c : p.blackCards){
							if(c.getColor()==3) return true;
						}
					}else if(getOwner().redCards.size()==2){
						for(Card c : p.blackCards){
							if(c.getColor()==3 || c.getColor()==5) return true;
						}
					}else if(getOwner().blueCards.size()==2){
						for(Card c : p.blackCards){
							if(c.getColor()==3 || c.getColor()==4) return true;
						}
					}else{
						if (p.blackCards.size()>0) return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		bf.waitfor(1000);
		Player target=bf.roleMap.get(tvo.target);
		Card card;
		if(tvo.cards==null){
			tvo.cards = new ArrayList<Integer>();
		}
		if(tvo.cards.size()==0){
			ArrayList<Card> selectable=new ArrayList<Card>();
			if(getOwner().redCards.size()==2 && getOwner().blueCards.size()==2){
				for(Card c : target.blackCards){
					if(c.getColor()==3) selectable.add(c);
				}
			}else if(getOwner().redCards.size()==2){
				for(Card c : target.blackCards){
					if(c.getColor()==3 || c.getColor()==5) selectable.add(c);
				}
			}else if(getOwner().blueCards.size()==2){
				for(Card c : target.blackCards){
					if(c.getColor()==3 || c.getColor()==4) selectable.add(c);
				}
			}else{
				for(Card c : target.blackCards){
					selectable.add(c);
				}
			}
//			int random = (int)(Math.random()*(target.blackCards.size()));
//			card = target.blackCards.get(random);
			int random = (int)(Math.random()*(selectable.size()));
			card = selectable.get(random);
		} else card=bf.cardsMap.get(tvo.cards.get(0));
//		card.setOwner(target);
		card.setOwner(getOwner());
		
		target.removeCardFromInfo(card, false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ArrayList<Card> temp=new ArrayList<Card>();
		temp.add(card);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",4);//type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5手卡到牌库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.putNumber("from",target.getUid());
		resp.putNumber("to",getOwner().getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(getOwner(), temp, 1);
		
		resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putBool("turnRole", true);
		resp.putNumber("oid",bf.operId);
		bf.SendToALL(resp);
		bf.waitfor(1000);
		getOwner().setIsOpen(false);
		
		excute();
	}
	
	@Override
	public void excute() {
		bf.usedCardStack.removeLast();
	}
}
