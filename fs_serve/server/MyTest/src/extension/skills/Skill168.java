package extension.skills;
import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *天罗地网:当你获得传出的情报时，你可以翻开此角色牌，然后追踪（2）：一张直达。
 */
public class Skill168 extends Skill {
	@Override
	public Boolean check() {
		if(getOwner().getIsDead() || getOwner().getIsOpen())return false;
		if(!disabled  && bf.nowStep==StepCons.InfoReceive && bf.thirdStep!=StepCons.Victory && bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).typeOfGetting==0 && bf.nowGetCardPlayer.getUid()==getOwner().getUid()){
			if(bf.nowGetCards.get(0).getOwner()==bf.nowPlayer){
				return true;
			}
		}
		return false;
	}

	int i=0;//最终展示的总张数
	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
//		playAni(true);
		ArrayList<Card> cards=new ArrayList<Card>();
		for(int j=0;j<2;j++){
			cards.addAll(bf.getCardFromCardPack(1)); //Remove a card from the top of pack and add it into "cards".
			i++;
			if(cards.get(i-1).getSend()==1)break;//If it is Capture, break.
		}
		
//		bf.cards.addAll(0, cards); //Optimization required
		ActionscriptObject arr1=new ActionscriptObject();
		bf.setCardsResp(arr1, cards);
	
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("rid", getOwner().getRoleId());
		resp.putNumber("numOfShowed", i);
		resp.put("cards",arr1);
		bf.SendToALL(resp);
//		bf.sResult.dispose();
		bf.waitfor(1000+(i+1)*1000+1000);//等动画做好后用这个
//		bf.waitfor(1000+2000+1000);
		
//		bf.drawCard(getOwner(), bf.getCardFromCardPack(i), 1, null); //Optimization required
//		bf.waitfor(1500);
		getOwner().addToHand(cards);
		i=0;
	}
	
}
