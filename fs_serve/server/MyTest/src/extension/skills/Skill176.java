package extension.skills;

import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.ReflectVO;
import extension.vo.SelectVO;
import extension.vo.SkillVO;

/**顺水推舟：[触发技]当一位玩家获得你传出的情报时，你可以展示牌库顶的１张牌。若该牌与该玩家获得的情报的颜色完全相同，将其放置到该玩家面前；否则，你获得该展示牌并盖伏此角色牌。**/
public class Skill176 extends Skill {

	@Override
	public Boolean check() {
		if(getOwner().getIsDead() || !getOwner().getIsOpen() || getExclusiveSkill().launchCount>0)return false;
		if(bf.nowStep==StepCons.InfoReceive && bf.thirdStep!=StepCons.Victory && bf.skillstep==0 && bf.nowGetCards.size()>0 && bf.nowGetCardPlayer!=null && bf.nowPlayer==getOwner() && bf.nowGetCards.get(0).typeOfGetting==0){
			return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		getExclusiveSkill().launchCount++;
		super.play(tvo);
		Player target = bf.roleMap.get(this.nowGetCardPlayerUid);
		Card c = this.infosToBeReceived.get(this.infosToBeReceived.size()-1);
		ArrayList<Card> cards=bf.getCardFromCardPack(1);
		boolean isSameColor = c.getColor()==cards.get(0).getColor()?true:false;
//		bf.cards.addAll(0, cards);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, cards);
		tvo.dur=10000;
		tvo.target=this.nowGetCardPlayerUid;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.put("cards",arr);
		resp.putBool("isSameColor", isSameColor);
		resp.putBool("targetDied", target.getIsDead());
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		bf.SendToALL(resp);
		bf.waitfor(isSameColor?4500:(target.getIsDead()?3500:6000));
		if(isSameColor){
			if(target.getIsDead()){
				bf.cards.addAll(0, cards);
				bf.updateDeck();
			}else{
				cards.get(0).setOwner(getOwner());
				bf.getCard(target, cards, 5);
			}
		}else{
//			getOwner().addToHand(cards); //获得展示牌版本
			bf.cards.addAll(0, cards); //不获得展示牌版本
			bf.updateDeck();
			getOwner().setIsOpen(false);
		}
	}
}
