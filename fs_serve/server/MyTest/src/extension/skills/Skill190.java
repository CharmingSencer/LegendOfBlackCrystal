package extension.skills;
import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *胁迫：[响应技]发动：翻开此角色牌并指定一位其他玩家。结算：令该玩家在你面前放置一张你指定颜色（红，蓝或黑）的情报。若其拒绝或不能如此做，你可以弃其1张手牌并可以在其面前放置一张黑情报。
 */
public class Skill190 extends Skill {

	@Override
	public Boolean check() {
		if(!getOwner().getIsOpen() && !getOwner().getIsDead() && this.blueSkillCheck()){
			return true;
		}
		return false;
	}
	
	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().setIsOpen(true);
		playAni(true);
		bf.waitfor(1500);
	}
	
	@Override
	public void excute() {
		Player target = bf.roleMap.get(getTvo().target);
		if(getOwner().getIsDead() || target.getIsDead() || (target.handcards.size()==0 && !this.hasCardColor(6, getOwner()))){
			return;
		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn", true);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000); 
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){ //svo.type 1蓝色 2红色 3黑色
		if(svo.type==0)svo.type=3;
//		String color;
//		switch(svo.type){
//		case 1:
//			color = "蓝色";
//			break;
//		case 2:
//			color = "红色";
//			break;
//		case 3:
//			color = "黑色";
//			break;
//		}
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		getTvo().dur=10000;
		getTvo().setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putBool("goOn2", true);
		resp.putNumber("color", svo.type);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(10000); 
		userSelected1(bf.sResult);
	}
	
	private void userSelected1(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards!=null && svo.type==0){ //放置
			Card c = bf.cardsMap.get(svo.cards.get(0));
			ArrayList<Card> temp = new ArrayList<Card>();
			temp.add(c);
			target.removeCardFromHand(c,false);
			ActionscriptObject resp=new ActionscriptObject();
			ActionscriptObject arr=new ActionscriptObject();
			bf.setCardsResp(arr, temp);
			resp.putNumber("from",target.getUid());
			resp.putNumber("to", getOwner().getUid());
			resp.putNumber("type",3); //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
			resp.put("cards",arr);
			resp.putNumber("h",2);
			resp.putNumber("f",27);
			bf.SendToALL(resp);
			bf.waitfor(2000);
			bf.getCard(getOwner(),temp,1);
		}else{//拒绝
			if(target.handcards.size()>0){
				ActionscriptObject resp=new ActionscriptObject();
				ActionscriptObject obj=new ActionscriptObject();
				getTvo().dur=10000;
				getTvo().setResponse(obj);
				resp.put("tvo",obj);
				resp.putNumber("h",2);
				resp.putNumber("f",25);
				resp.putNumber("oid",bf.operId);
				resp.putBool("discard", true);
				resp.putNumber("num",target.handcards.size());
				bf.SendToALL(resp);
				bf.sResult.dispose();
				bf.waitfor(10000); 
				discardResult(bf.sResult);
			}
			if(this.hasCardColor(6, getOwner())==false){
				return;
			}else{
				ActionscriptObject resp1=new ActionscriptObject();
				ActionscriptObject obj1=new ActionscriptObject();
				getTvo().dur=10000;
				getTvo().setResponse(obj1);
				resp1.put("tvo",obj1);
				resp1.putNumber("h",2);
				resp1.putNumber("f",25);
				resp1.putNumber("oid",bf.operId);
				resp1.putBool("placeBlack", true);
				bf.SendToALL(resp1);
				bf.sResult.dispose();
				bf.waitfor(10000); 
				userSelected2(bf.sResult);
			}
		}
	}
	
	private void discardResult(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		if(svo.cards==null){
//			svo.cards=new ArrayList<Integer>();
//			int random = (int)Math.random()*target.handcards.size();
//			svo.cards.add(random);
			return;
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		Card c=target.handcards.get(svo.cards.get(0));
		temp.add(c);
		bf.disCard(target, temp, 1,null);
		bf.waitfor(2000);
	}
	
	public void userSelected2(SelectVO svo){		
		if(svo.cards==null)return;
		Player target = bf.roleMap.get(getTvo().target);
		Card c = bf.cardsMap.get(svo.cards.get(0));
		ArrayList<Card> temp = new ArrayList<Card>();
		temp.add(c);
		getOwner().removeCardFromHand(c,false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, temp);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to", target.getUid());
		resp.putNumber("type",3); //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		bf.getCard(target,temp,1);
	}
	
}
