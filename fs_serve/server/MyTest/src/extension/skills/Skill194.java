package extension.skills;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *谣言四起:[触发技]当你获得红蓝情报时，你可以将你面前的一张与该情报含有不同颜色的情报转置到一位其他玩家面前。
 */
public class Skill194 extends Skill {

	@Override
	public Boolean check() {
		int color = 0;
		if(!getOwner().getIsDead() && bf.nowGetCards.size()==1 && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer==getOwner() && bf.thirdStep==StepCons.AfterDead){
			for(Card c:bf.nowGetCards){
				if(c.getColor()!=3){
					color = c.getColor();
//					ArrayList<Integer> difColor = new ArrayList<Integer>();//装 含有与该情报含有不同颜色的卡牌的颜色
//					switch(color){
//					case 0:
//						return false;
//					case 1://蓝
//						difColor.add(2);
//						difColor.add(3);
//						difColor.add(4);
//						difColor.add(5);
//						break;
//					case 2://红
//						difColor.add(1);
//						difColor.add(3);
//						difColor.add(4);
//						difColor.add(5);
//						break;
//					case 3://黑
//						difColor.add(1);
//						difColor.add(2);
//						difColor.add(4);
//						difColor.add(5);
//						break;
//					case 4://红黑
//						difColor.add(1);
//						difColor.add(5);
//						break;
//					case 5://蓝黑
//						difColor.add(2);
//						difColor.add(4);
//						break;
//					}
					
					//以上步骤由下面的代码代替
					if(color>=1 && color <=2){
						for(Card card:getOwner().infocards){
							if(card.getColor()!=color) return true;
						}
					}else if(color>=4 && color <=5){
						for(Card card:getOwner().infocards){
							if(card.getColor()!=color && card.getColor()!=6-color && card.getColor()!=3) return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public void play(SkillVO tvo) {
		if(getOwner().getIsDead())return;
		super.play(tvo);
		/*以下代码因技能改版废弃
		Set<Integer> colorRecv = new HashSet<Integer>(); //收到的情报的颜色集合
		for(Card c : this.infosToBeReceived){
			if(c.getColor() != 3){
				colorRecv.add(c.getColor());
			}
		}
		Boolean found = false; //情报区是否有与收到的情报含有不同颜色的情报
		ArrayList<Card> availableInfos = new ArrayList<Card>(); //可以转置的情报
		//判断是否还满足结算条件，同时将可以转置的情报放入数组。
		for(int i : colorRecv){
			if(i>=1 && i <=2){
				for(Card card:getOwner().infocards){
					if(card.getColor()!=i){
						found = true;
						if(availableInfos.indexOf(card)<0){
							availableInfos.add(card);
						}
					}
				}
			}else if(i>=4 && i <=5){
				for(Card card:getOwner().infocards){
					if(card.getColor()!=i && card.getColor()!=6-i && card.getColor()!=3){
						found = true;
						if(availableInfos.indexOf(card)<0){
							availableInfos.add(card);
						}
					}
				}
			}
		}
		if(found == false) return;
		*/
		
		Boolean found = false; //情报区是否有与收到的情报含有不同颜色的情报
		Card c = this.infosToBeReceived.get(0);
		int color = c.getColor();
		ArrayList<Card> availableInfos = new ArrayList<Card>(); //可以转置的情报
		if(color>=1 && color <=2){
			for(Card card:getOwner().infocards){
				if(card.getColor()!=color){
					found = true;
//					if(availableInfos.indexOf(card)<0){
						availableInfos.add(card);
//					}
				}
			}
		}else if(color>=4 && color <=5){
			for(Card card:getOwner().infocards){
				if(card.getColor()!=color && card.getColor()!=6-color && card.getColor()!=3){
					found = true;
//					if(availableInfos.indexOf(card)<0){
						availableInfos.add(card);
//					}
				}
			}
		}
		if(found==false) return; //自己面前已经没有可以转置的请报了
		
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		bf.setCardsResp(arr, availableInfos);
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.put("cards", arr);
		bf.SendToALL(resp);
		bf.sResult.dispose();
		bf.waitfor(11500);
		userSelected(bf.sResult);
	}
	
	private void userSelected(SelectVO svo){
		Player target = bf.roleMap.get(getTvo().target);
		Card card;
		if(svo.cards==null){
			svo.cards = new ArrayList<Integer>();
			int random = (int)(Math.random()*(getOwner().infocards.size()));
			card=getOwner().infocards.get(random);
		}else card=bf.cardsMap.get(svo.cards.get(0));
		card.setOwner(getOwner());
		
		getOwner().removeCardFromInfo(card, false);
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject ca=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		card.setResponse(ca);
		arr.put(0,ca);
		resp.putNumber("from",getOwner().getUid());
		resp.putNumber("to",target.getUid());
		resp.putNumber("type",4);  //type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5手卡到牌库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.put("cards",arr);
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		
		ArrayList<Card> cards=new ArrayList<Card>();
		cards.add(card);
		bf.getCard(target, cards, 1);	
//		bf.waitfor(2000);
	}
}
