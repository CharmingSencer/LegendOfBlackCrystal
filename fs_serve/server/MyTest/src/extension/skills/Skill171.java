package extension.skills;
import extension.cons.StepCons;
import extension.vo.SkillVO;
import extension.vo.TargetVO;
/**
 *薪尽火传/弹药收集 其他玩家使用的黑色烧毁结算后加入你的手牌。
 */
public class Skill171 extends Skill {
	public Skill171(){
		auto=true;
	}
	@Override
	public Boolean check() {
		return (bf.thirdStep==0 && !getOwner().getIsDead() && bf.subStep==StepCons.CardSettlementEnd && ((TargetVO)bf.nowSettlement).cid==3 && ((TargetVO)bf.nowSettlement).color >=3 && ((TargetVO)bf.nowSettlement).sponsor!=getOwner().getUid());
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		playAni(false);
		bf.waitfor(1500);
		if(bf.nowSettlement == this.settledCard){
			((TargetVO)bf.nowSettlement).moveto=getOwner().getUid();
			getOwner().addToHand(bf.cardsMap.get(((TargetVO)bf.nowSettlement).cvid));
		}
	}
	
}
