package extension.skills;
import java.util.ArrayList;

import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *霸权：你的回合结束时,再次进入你的回合
 */
public class Skill128 extends Skill {

	@Override
	public Boolean check() {
		return ( bf.nowStep==StepCons.StepEnd && getOwner().skillhash.get(this.id).launchCount<1 && bf.nowPlayer==getOwner()
				&& getOwner().handcards.size()>=2);
	}
	
	@Override
	public void reset() {
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
		getOwner().skillhash.get(this.id).launchCount++;
		playAni(false);
		bf.sResult.dispose();
		bf.waitfor(13*1000);
		DiscardResult(bf.sResult);
	}
	
	public void DiscardResult(SelectVO svo){
		if(svo.cards==null){//如果是null，则自动选择弃的牌
			svo.cards=new ArrayList<Integer>();
		}	
		if(svo.cards.size()<2){
			for(Card c:getOwner().handcards){
				if(svo.cards.indexOf(c.getVid())<0){
					svo.cards.add(c.getVid());
					if(svo.cards.size()==2)break;
				}
			}
		}
		ArrayList<Card> temp=new ArrayList<Card>();
		for(Integer i:svo.cards){
			temp.add(bf.cardsMap.get(i));
		}
		bf.disCard(getOwner(), temp, 1, null);
		bf.waitfor(2000);
		
		bf.nextPlayer=getOwner();
	}
}
