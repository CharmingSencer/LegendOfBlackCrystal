package extension.skills;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
import extension.vo.TargetVO;
import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
/**
 *授业 你使用的黑色功能牌(调包除外)结算后，你可以将该牌交给一位其他玩家。（每回合限发动一次）
 */
public class Skill172 extends Skill {
//	public Skill172(){
//		auto=true;
//	}
	@Override
	public Boolean check() {
		if(bf.thirdStep==0 && !getOwner().getIsDead() && bf.subStep==StepCons.CardSettlementEnd && bf.nowSettlement instanceof TargetVO && ((TargetVO)bf.nowSettlement).color>=3 && ((TargetVO)bf.nowSettlement).cid!=10 && ((TargetVO)bf.nowSettlement).sponsor==getOwner().getUid()){
			if(getOwner().skillhash.get(this.id).launchCount<1) return true;
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
//		SmartFoxServer.log.info("bf.nowSettlement.cid == "+((TargetVO)bf.nowSettlement).cid);
//		SmartFoxServer.log.info("this.settledCard.cid == "+this.settledCard.cid);
//		if(bf.nowSettlement == this.settledCard){
			super.play(tvo);
			playAni(false);
			getOwner().skillhash.get(this.id).launchCount++;
			bf.sResult.dispose();
			bf.waitfor(11500);
			userSelected(bf.sResult);
//		}
	}
	
	private void userSelected(SelectVO svo){
		if(svo.target==0){
			svo.target=bf.getNextPlayer(getOwner()).getUid();
		}
		((TargetVO)bf.nowSettlement).moveto=svo.target;
		Player Target = bf.roleMap.get(svo.target);
		Target.addToHand(bf.cardsMap.get(((TargetVO)bf.nowSettlement).cvid));
		
//		Card c=bf.cardsMap.get(((TargetVO)bf.nowSettlement).cvid);
//		if(c.getId() >= 13 && c.getId() <= 18){
//			getTvo().target = svo.target;
//			ActionscriptObject resp=new ActionscriptObject();
//			ActionscriptObject obj=new ActionscriptObject();
//			ActionscriptObject card=new ActionscriptObject();
//			getTvo().setResponse(obj);
//			resp.put("tvo",obj);
//			c.setResponse(card);
//			resp.put("probe",card);
//			resp.putNumber("h",2);
//			resp.putNumber("f",25);
//			resp.putNumber("oid",bf.operId);
//			bf.SendToALL(resp);
//		}
	}
}
