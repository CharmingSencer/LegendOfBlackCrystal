package extension.skills;
import java.util.ArrayList;

import it.gotoandplay.smartfoxserver.SmartFoxServer;
import it.gotoandplay.smartfoxserver.lib.ActionscriptObject;
import extension.Player;
import extension.cards.Card;
import extension.cons.StepCons;
import extension.vo.SelectVO;
import extension.vo.SkillVO;
/**
 *碎裂:[触发技]当你获得来源为其他玩家的黑情报时，你可以翻开此角色牌，令该玩家抽２张牌，然后你将其面前的一张红蓝情报收入你的手牌。
 */
public class Skill182 extends Skill {
	@Override
	public Boolean check() {
		if(getOwner().getIsDead() || getOwner().getIsOpen())return false;
//		if(bf.nowStep==StepCons.InfoReceive && bf.skillstep==0	&& bf.nowGetCardPlayer==getOwner() && bf.nowPlayer!=getOwner()
//				&& bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).typeOfGetting==0){
		if(bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).getOwner()!=getOwner() && bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer==getOwner() && bf.thirdStep==StepCons.AfterDead){
			for(Card c:bf.nowGetCards){
				if(c.getColor()>2)return true;
			}
		}
		return false;
	}

	@Override
	public void play(SkillVO tvo) {
		super.play(tvo);
//		tvo.target=bf.nowPlayer.getUid();
//		Player target = bf.nowPlayer;
		Player target = this.infosToBeReceived.get(0).getOwner();
		tvo.target = target.getUid();
		getOwner().setIsOpen(true);
		boolean hasRB = (hasInfoColor(1, target)|| hasInfoColor(2, target));
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject obj=new ActionscriptObject();
		tvo.dur=10000;
		tvo.setResponse(obj);
		resp.put("tvo",obj);
		resp.putNumber("h",2);
		resp.putNumber("f",25);
		resp.putNumber("oid",bf.operId);
		resp.putNumber("rid", getOwner().getRoleId());
		bf.SendToALL(resp);
		bf.waitfor(1000);
		bf.drawCard(target, bf.getCardFromCardPack(1), 1, null);
		bf.waitfor(1000);
		if(hasRB){
			resp.removeElement("oid");
			resp.putNumber("oid",bf.operId);
			resp.putBool("hasRB", hasRB);
			bf.SendToALL(resp);
			bf.sResult.dispose();
			bf.waitfor(10000);
			userSelected(bf.sResult);		
		}
	}
	
	private void userSelected(SelectVO svo){
		Player target=bf.roleMap.get(getTvo().target);
		//If the user did not select a target message, pick a random one for him.
		if(svo.cards==null){
			svo.cards=new ArrayList<Integer>();
			int random = (int)(Math.random()*(target.infocards.size()-1));
			Card card =target.infocards.get(random);
			svo.cards.add(card.getVid());
		}
	
		ActionscriptObject resp=new ActionscriptObject();
		ActionscriptObject arr=new ActionscriptObject();
		ArrayList<Card> temp=new ArrayList<Card>();
		Card car = target.removeCardFromInfo(svo.cards.get(0), false);
		temp.add(car);
		bf.setCardsResp(arr, temp);
		resp.put("cards",arr);
		resp.putNumber("type",2);//type 1手卡到手卡 2 情报到手卡 3手卡到情报 4情报到情报 5情报到库顶 6情报到牌库顶 7情报到弃牌堆 8从手卡到传递中的情报 9从牌库顶到情报
		resp.putNumber("from",target.getUid());
		resp.putNumber("to",getOwner().getUid());
		resp.putNumber("h",2);
		resp.putNumber("f",27);
		bf.SendToALL(resp);
		bf.waitfor(2000);
		getOwner().addToHand(car); //Place this statement after the animation to make it visible to global.
	}
	
}
