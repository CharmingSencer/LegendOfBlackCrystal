package extension.tasks;

import extension.cons.StepCons;

/**
 *一位其他玩家在其回合死亡
 */
public class Task54 extends TaskBase{
	@Override
	public Boolean check() {
		if(bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.getIsDead() && bf.nowPlayer==bf.nowGetCardPlayer && bf.nowGetCards.size()>0  && bf.thirdStep!=StepCons.AfterDeadTaskCheck){
			return true;
		}
		return false;
	}
}
