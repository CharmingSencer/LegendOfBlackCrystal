package extension.tasks;
/**
 *一位女性宣告胜利
 */
public class Task14 extends TaskBase{
	@Override
	public Boolean check() {
		if(bf.victoryMan!=null && !owner.getIsDead()){
			return bf.victoryMan.sex!=1;
		}
		return super.check();
	}
}
