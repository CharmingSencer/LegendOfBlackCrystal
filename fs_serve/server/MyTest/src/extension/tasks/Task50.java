package extension.tasks;

import it.gotoandplay.smartfoxserver.SmartFoxServer;

/**
 *获得两张红情报和两张蓝情报。
 */
public class Task50 extends TaskBase{

	@Override
	public Boolean check() {
		if(!owner.getIsDead()){
			return (owner.blueCards.size()>=2 && owner.redCards.size()>=2);
		}
		return false;
	}

}
