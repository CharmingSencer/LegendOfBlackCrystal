package extension.tasks;

import extension.Player;

/**
 *【顺水推舟】放置的情报令目标玩家宣胜或令其成为第二位或以后死亡的玩家。
 */
public class Task45 extends TaskBase{
	@Override
	public Boolean check() {
		if(bf.victoryMan!=null && bf.nowGetCardPlayer!=null && bf.victoryMan.getUid()==bf.nowGetCardPlayer.getUid() && bf.nowGetCards.size()>0 && bf.nowGetCards.get(0).bySSTZ){		
			return true;
		}
		
		if(bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.getIsDead() && !owner.getIsDead()){
			if(bf.nowGetCards.get(0).bySSTZ && bf.nowGetCards.get(0).getOwner()==owner){
				int c=0;
				for(Player p:bf.roleSeq){
					if(p.getIsDead())c++;
				}
				return c>=2;
			}
		}
		
		return super.check();
	}
}
