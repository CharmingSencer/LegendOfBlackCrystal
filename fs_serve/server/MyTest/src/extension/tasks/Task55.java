package extension.tasks;

import extension.Player;
import extension.cons.StepCons;

/**
 *亲手杀死一位酱油玩家，或者潜伏与军情其中一方全灭
 */
public class Task55 extends TaskBase{
	@Override
	public Boolean check() {
		if(bf.nowGetCardPlayer!=null && bf.nowGetCardPlayer.getIsDead() && !owner.getIsDead()){
			if(bf.nowGetCards.get(0).getOwner()==owner){
				int c=0;
				for(Player p:bf.roleSeq){
					if(p.getIsDead())c++;
				}
				return c==1;
			}
		}	
		
		if(bf.thirdStep==StepCons.AfterDeadTaskCheck || bf.thirdStep==StepCons.Victory){
			int a=0;
			int b=0;
			for(Player p:bf.roleSeq){
				if(!p.getIsDead() && p.getIndentity()==0)a++;
				if(!p.getIsDead() && p.getIndentity()==1)b++;
			}
		return (a==0 || b==0);
		}
		return false;
	}
}
