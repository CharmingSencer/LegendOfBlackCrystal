package extension.tasks;

import extension.cards.Card;
import it.gotoandplay.smartfoxserver.SmartFoxServer;

/**
 *获得三张或以上的纯红情报
 */
public class Task51 extends TaskBase{

	@Override
	public Boolean check() {
		if(!owner.getIsDead()){
			int sr = 0;//solid red
			for(Card c : owner.redCards){
				if(c.getColor()==2) ++sr;
			}
			return sr>=3;
		}
		return false;
	}

}
