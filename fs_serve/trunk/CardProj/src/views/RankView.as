package views
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	
	import core.mng.Evt;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	
	import events.WEvent;
	
	import game.ui.test.RankUI;
	
	import handlers.RoomHandler;
	
	import manage.BM;
	
	import util.GUtil;
	
	public class RankView extends RankUI
	{
		private var tempdata:Array;
		private var currentCategory:String = "coin"; //类别 1积分 2等级 3杀人数 4死亡数
		private var btns:Array = ["rating","lvl","kill","death"];
		public function RankView()
		{
			super();
			Evt.add(RoomHandler.onRank,onRank);
			btnsListener(true);
			closebtn.addEventListener(MouseEvent.CLICK,onclosebtnclick);
			tempdata=new Array();
		}
		
		protected function onclosebtnclick(event:MouseEvent):void
		{
			this.close();
		}
		
		private function btnsListener(boo:Boolean):void
		{
			if(boo){
				for(var i:int in btns){
					this[btns[i]].addEventListener(MouseEvent.CLICK,onBtnClick);
				}
			}else{
				for(var i:int in btns){
					this[btns[i]].removeEventListener(MouseEvent.CLICK,onBtnClick);
				}
			}
		}
		
		protected function onBtnClick(event:MouseEvent):void
		{
			if(event.currentTarget.name == this.currentCategory) return;
			App.log.info("btn:"+event.currentTarget.name);
			switch(event.currentTarget.name)
			{
				case "rating":  //托管
					this.currentCategory = "coin";
					break;
				case "lvl":
					this.currentCategory = "exp";
					break;
				case "kill":
					this.currentCategory = "killCount";
					break;
				case "death":
					this.currentCategory = "deathCount";
					break;
			}
			this.refreshData();
		}
		
		
		//向服务器发送查询指令
		public function showData():void{
			if(!GL.rankdata0){
//				App.log.info("category:"+this.currentCategory);
//				SFS.inst.sfs.sendXtMessage(Cons.extension.roomserv,Cons.cmd.CheckRank,{category:this.currentCategory}); //0.172
				SFS.inst.sfs.sendXtMessage(Cons.extension.roomserv,Cons.cmd.CheckRank,null);
			}
		}
		
		//接收到服务器查询结果
		private function refreshData():void
		{
			App.log.info("category:"+this.currentCategory);
			switch(this.currentCategory){
				case "coin":
					rlist.dataSource=GL.rankdata0;
					this.attri.text="积分";
					break;
				case "exp":
					rlist.dataSource=GL.rankdata1;
					this.attri.text="等级";
					break;
				case "killCount":
					rlist.dataSource=GL.rankdata2;
					this.attri.text="杀人数";
					break;
				case "deathCount":
					rlist.dataSource=GL.rankdata3;
					this.attri.text="死亡数";
					break;
			}
			rlist.refresh();
//			for(var i:int = 0; i < rlist.length; i++){
//				rlist[i]["coin"] = rlist.dataSource[i][this.currentCategory];
//			}
		}
		
		//接收到服务器查询结果
		private function onRank(evt:WEvent):void
		{
			if(evt.param.ranks0){
				GL.rankdata0=evt.param.ranks0 as Array; //array of obj's
				rlist.dataSource=GL.rankdata0;
			}
			if(evt.param.ranks1){
				GL.rankdata1=evt.param.ranks1 as Array; //array of obj's
				for (var i:int in GL.rankdata1){
					GL.rankdata1[i]["coin"]=GUtil.getLevelByExp(GL.rankdata1[i]["coin"])+"级";
				}
//				rlist.dataSource=GL.rankdata1;
			}
			if(evt.param.ranks2){
				GL.rankdata2=evt.param.ranks2 as Array; //array of obj's
//				rlist.dataSource=GL.rankdata3;
			}
			if(evt.param.ranks3){
				GL.rankdata3=evt.param.ranks3 as Array; //array of obj's
//				rlist.dataSource=GL.rankdata4;
			}
		}
		
//		//笔记：http://layaflash.ldc.layabox.com/html/2015/mornui_1030/39.html
		//List.renderHandler的使用方法：
//		list.renderHandler = new Handler(listRender);//自定义渲染方式
//		
//		list.array = ["label1", "label2", "label3", "label4", "label5", "label6"];//赋值
//		
//		/**自定义List项渲染*/
//		private function listRender(item:Component, index:int):void 
//		{
//			if (index < list.length) 
//			{
//				var icon:Clip = item.getChildByName("icon1") as Clip;
//				icon.frame = index + 1;
//				var label:Label = item.getChildByName("label1") as Label;
//				label.text = list.array[index];
//			}
//		}
	}
}