package skills
{
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Evt;
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	import datas.Trans;
	
	import handlers.BattleHandler;
	
	import utils.OBJUtil;
	import utils.Rand;
	
	import views.ACard;

		/**
		 * 掩饰
		 */	
		public class Skill118 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playYellowAni();
				App.audio.play(Cons.audio.YELLOW);
//				TweenMax.delayedCall(3,delayedFun);
				TweenMax.delayedCall(3,delayedFun1);
			}
//			override public function delayedFun():void
//			{
//				if(GL.id!=tvo.sponsor){
//					data.to=tvo.sponsor;
//					data.num=1;
//					data.cards=null;
//				}
//				data.type=1;
//				Evt.dipatch(BattleHandler.onAddCard,data);
//				TweenMax.delayedCall(1,delayedFun1);
//			}
			
			public function delayedFun1():void{
				var p:Player=phash.get(tvo.sponsor);
				var temp:Array=[];
				if(GL.id==p.uid){
					var cards:Array = this.data.cards;
					for(var i:int in cards){
						var c:ACard=new ACard();
						c.setdata(cards[i]);
						temp.push(c);
					}
				}else{
					var c:ACard=new ACard();
					c.bg.bitmapData=OBJUtil.getInstanceByClassName("Act0") as BitmapData;
					temp.push(c);
				}
				
				bm.addCardsToCenter(temp,true);//展示牌1秒
				TweenMax.delayedCall(1,function(arr:Array):void{
//					if(p.uid==GL.id){
						var targetPoint:Point=p.view.localToGlobal(new Point(p.view.g1.x,p.view.g1.y));	
						for(var i:int in arr){
							TweenMax.to(arr[i],0.8,{x:targetPoint.x,y:targetPoint.y,scaleX:0.1,scaleY:0.1,onComplete:delayedFun2,onCompleteParams:[arr[i]]});
						}
//					}else{
//						var targetPoint:Point=p.view.localToGlobal(new Point(p.view.g1.x,p.view.g1.y));	
//						for(var i:int in arr){
//							TweenMax.to(arr[i],0.8,{x:targetPoint.x,y:targetPoint.y,scaleX:0.1,scaleY:0.1,onComplete:delayedFun2,onCompleteParams:[arr[i]]});
//						}
//					}
				},[temp]);
			}
			
			public function delayedFun2(ca:ACard):void{
				bv.removeChild(ca);
				var p:Player=phash.get(tvo.sponsor);
				if(GL.id==p.uid){
					p.iden=data.newiden;
				}else{
					p.view.g1.skin=Trans.getSkinByIdentity(3);
					p.view.g1.mouseEnabled=true;
				}
			}
		}
}