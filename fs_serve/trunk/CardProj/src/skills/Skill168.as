package skills
{
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	import datas.TargetVO;
	
	import events.WEvent;
	
	import morn.core.components.Clip;
	
	import util.GUtil;
	
	import utils.ArrayUtil;
	import utils.HashMap;
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;

		/**
		 * 天罗地网
		 */	
		public class Skill168 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
//				if(obj.goOn){
//					delayedFun1();
//				}else{
					playTurnAni(0, obj.rid);
					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,3)+".mp3");
					App.audio.play(Cons.audio.TurnRole);
					TweenMax.delayedCall(1,delayedFun);
//				}
			}
			
			override public function delayedFun():void
			{
				var p:Player=phash.get(tvo.sponsor);
				p.rid=data.rid;
				var cards:Array=data.cards;
				App.log.info("data.numOfShowed="+data.numOfShowed);
				if(data.numOfShowed==1){
					var temp:Array=[];
					for(var i:int in cards){
						var c:ACard=new ACard();
						c.setdata(cards[i]);
						temp.push(c);
					}
//					p.view.showTime(tvo.dur);
					bm.addCardsToCenter(temp,true);
					TweenMax.delayedCall(2,getCards,[temp]);
				}else if(data.numOfShowed==2){
					var temp2:Array=[];
					for(var j:int in cards){
						var ca:ACard=new ACard();
						ca.setdata(cards[j]);
						temp2.push(ca);
					}
//					p.view.showTime(tvo.dur);
					App.log.info("temp2.length="+temp2.length);
					showFirst(temp2);
				}
			}
			
			public function showFirst(arr:Array):void{
				//以下为将追踪的第一张牌添加入屏幕中间
				var c0:ACard=arr[0] as ACard;
				var cpoint:Array=OBJUtil.getCenterPos(Cons.BWID,Cons.BHEI,1,Cons.CWID,Cons.CHEI);
				c0.x=cpoint[0].x;
				c0.y=cpoint[0].y;
				TweenNano.from(c0,0.8,{alpha:0});
				bv.addChild(c0);
				var temp:Array = [];
				temp.push(c0);
				temp.push(arr[1])
				TweenMax.delayedCall(1,showSecond,[temp]);
				
			}
			
			public function showSecond(arr:Array):void{
				var c0:ACard=arr[0] as ACard;
				var c1:ACard=arr[1] as ACard;
				var centArr:Array=OBJUtil.getCenterPos(Cons.BWID,Cons.BHEI,arr.length,Cons.CWID,Cons.CHEI);
//				for each(var c:ACard in arr){
//					var param:Object={x:centArr[i].x,y:centArr[i].y,scaleX:1,scaleY:1,rotation:0};
//					TweenMax.to(arr[i],0.8,param);
//				}
				TweenMax.to(c0,0.2,{x:centArr[0].x,y:centArr[0].y});
				arr[1].x=centArr[1].x;
				arr[1].y=centArr[1].y;
				TweenNano.from(arr[1],0.8,{alpha:0});
				bv.addChild(arr[1]);
				TweenMax.delayedCall(1.5,getCards,[arr]);
			}
//			
			public function getCards(arr:Array):void{ //将展示的追踪到的牌从屏幕中间加入手牌
//				for  (var j:int in arr) 
//				{
//					bv.removeChild(arr[j]);
//				}
				var p:Player = phash.get(tvo.sponsor);
				bm.sendCardsToPoint(arr, p);
				App.audio.play(Cons.audio.Deal);
			}
			
			
//			private function onUseCard(evt:WEvent):void
//			{
//				clearState();
//				var tvo:TargetVO=new TargetVO();
//				tvo.setData(evt.param);
//				var target:Object;
//				var sponsor:Player=phash.get(tvo.sponsor) as Player;
//				var centArr:Array;
//				if(tvo.target>0)  //目标是玩家
//					target=phash.get(tvo.target);
//				else if(tvo.card>0){
//					for each(var o:ACard in usecardstack){
//						if(o.vid==tvo.card){
//							target=o;
//							break;
//						}
//					}
//				}
//				if(target && target is Player){
//					bv.setReport(StringUtils.getColorString("["+sponsor.pname+"]",0xffff00)+"对"+StringUtils.getColorString("["+target.pname+"]",0xffff00)+"使用了"+StringUtils.getColorString("["+GL.cdata.get(tvo.cid).n+"]",0x00ff00));
//					showArrow(sponsor,target);
//				}else if(target && target is ACard){
//					bv.setReport(StringUtils.getColorString("["+sponsor.pname+"]",0xffff00)+"对"+StringUtils.getColorString("["+target.cname+"]",0xffff00)+"使用了"+StringUtils.getColorString("["+GL.cdata.get(tvo.cid).n+"]",0x00ff00));
//					showArrow(sponsor,target);
//				}else{
//					bv.setReport(StringUtils.getColorString("["+sponsor.pname+"]",0xffff00)+"使用了"+StringUtils.getColorString("["+GL.cdata.get(tvo.cid).n+"]",0x00ff00));
//				}
//				
//				if(tvo.sponsor==GL.id){  //是自己发动的
//					var arr:Array=sponsor.hand;
//					var card:ACard;
//					for (var i:int=0;i<arr.length;i++){
//						if(arr[i].vid==tvo.cvid){
//							card=arr[i] as ACard;
//							if(card.id!=tvo.cid){
//								card.color=tvo.color;
//								card.id=tvo.cid;
//							}
//							card.canDiscover=tvo.canDiscover;
//							card.canuse=false;
//							arr.splice(i,1);
//							break;
//						}
//					}
//					usecardstack.push(card);
//					card.bv=bv;			
//					card.target=target;
//					sendCardsToCenter([card],sponsor);
//					GUtil.playCardSound(card,sponsor);
//				}else{
//					var p1:Point=sponsor.view.localToGlobal(new Point(sponsor.view.hcount.x,sponsor.view.hcount.y));
//					var ca:ACard=new ACard();
//					ca.color=tvo.color;
//					ca.id=tvo.cid;
//					ca.vid=tvo.cvid;
//					ca.bv=bv;			
//					ca.target=target;
//					ca.canDiscover=tvo.canDiscover;
//					usecardstack.push(ca);
//					sendCardsToCenter([ca],sponsor);
//					GUtil.playCardSound(ca,sponsor);
//				}
//				centArr=OBJUtil.getCenterPos(Cons.BWID,Cons.BHEI,usecardstack.length,Cons.CWID,Cons.CHEI);    //以下是把以前已经存在的卡布局
//				if(centArr.length>1){
//					var len:int=centArr.length-1;
//					for (var j:int = 0; j < len; j++) 
//					{
//						TweenMax.to(usecardstack[j],0.8,{x:centArr[j].x,y:centArr[j].y});					
//					}
//				}
//			}
			
			
		}
}