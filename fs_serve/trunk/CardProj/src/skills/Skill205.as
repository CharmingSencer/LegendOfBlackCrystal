package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;

		/**
		 * 筹谋
		 */	
		public class Skill205 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				if(obj.goOn){
					delayedFun1();
				}else{
					playNormalAni();
					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,5)+".mp3");
				}
			}
			public function delayedFun1():void{
				if(tvo.sponsor==GL.id){
					bv.showInfo("请选择你要放回牌库顶的一张手牌");
					var arr:Array=phash.get(GL.id).hand;
					cards=[];
					for each(var c:ACard in arr){
						cards.push(c);
					}
					this.showCards();
					phash.get(GL.id).view.showTime(tvo.dur-1000);
				}else{
					bv.showInfo("请等待沧海操作");
					var p:Player=phash.get(tvo.sponsor);
					p.view.showTime(tvo.dur-1000);
				}
			}
			override protected function onCardsSelect(evt:MouseEvent):void
			{
				var param:Object={tid:GL.tableId,ctype:1,type:1,card:evt.currentTarget.vid,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
				clearState();
			}
		}
}