package skills
{
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;

	/**
	 * 掩护
	 */
//	public class Skill17 extends Skill
//	{
//		override public function play(tvo1:SkillVO,obj:Object):void
//		{
//			super.play(tvo1,obj);
//				var p:Player=phash.get(tvo.sponsor);
//				playTurnAni(p.rid,0);
//				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+1+".mp3");
//				p.rid=0;
//		}
//	}
	
	public class Skill17 extends Skill
	{
			
		override public function launch():void
		{
			var arr:Array=phash.values();
			targets=[];
			var p:Player=phash.get(GL.id);
			if(p.red.length==2 && p.blue.length==2){
				for each(var i:Player in  arr){
					if(i.uid!=GL.id  &&  !i.isDead && !i.isLost && i.black.length>0){
						for each(var c:ACard in i.black){
							if(c.color==3){
								targets.push(i.view);
								break;
							}
						}
					}
				}
			}else if(p.red.length==2){
				for each(var i:Player in  arr){
					if(i.uid!=GL.id  &&  !i.isDead && !i.isLost && i.black.length>0){
						for each(var c:ACard in i.black){
							if(c.color==3 || c.color==5){
								targets.push(i.view);
								break;
							}
						}
					}
				}
			}else if(p.blue.length==2){
				for each(var i:Player in  arr){
					if(i.uid!=GL.id  &&  !i.isDead && !i.isLost && i.black.length>0){
						for each(var c:ACard in i.black){
							if(c.color==3 || c.color==4){
								targets.push(i.view);
								break;
							}
						}
					}
				}
			}else{
				for each(var i:Player in  arr){
					if(i.uid!=GL.id  &&  !i.isDead && !i.isLost && i.black.length>0){
							targets.push(i.view);
					}
				}
			}
			bv.showInfo("请选择一个玩家发动");
			this.showTarget();
		}	
		
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			clearState();
			var t:Player=phash.get(targetid);
			var p:Player=phash.get(GL.id)
			var temp:Array=[];

				if(p.red.length==2 && p.blue.length==2){
					for each(var c:ACard in t.black){
						if(c.color==3){
							var ac:ACard=new ACard();
							ac.setdata(c);
							temp.push(ac);
						}
					}
				}else if(p.red.length==2){
					for each(var c:ACard in t.black){
						if(c.color==1 || c.color==3 || c.color==5){
							var ac:ACard=new ACard();
							ac.setdata(c);
							temp.push(ac);
						}
					}
				}else if(p.blue.length==2){
					for each(var c:ACard in t.black){
						if(c.color==2 || c.color==3 || c.color==4){
							var ac:ACard=new ACard();
							ac.setdata(c);
							temp.push(ac);
						}
					}
				}else{
					for each(var c:ACard in t.black){
						var ac:ACard=new ACard();
						ac.setdata(c);
						temp.push(ac);
					}
				}
				
			
			bv.showCardDialog(temp,"请选择你要获得的情报",1,2,targetid,onselected);
		}
		
		public function onselected(arr:Array):void{
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,cards:arr,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
		}
		
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			var p:Player=phash.get(tvo.sponsor);
			if(obj.turnRole){
				playTurnAni(p.rid,0);
				p.rid=0;
			}else{
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+1+".mp3");
			}
		}
		
	}
}