package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import views.ACard;

	/**
	 * 岁月留痕
	 */	
	public class Skill93 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.goOn){
				delayedFun1();
			}else{
				var p:Player=phash.get(tvo.sponsor);
				playTurnAni(p.rid,0);
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+"1.mp3");
				p.rid=0;
				TweenMax.delayedCall(1, delayedFun);
			}
		}
		
		override public function delayedFun():void{
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(10000);
			if(tvo.sponsor==GL.id){
				bv.passView.setInfo("是否查看一位玩家手牌?",2,1,"否","是",passSelect);
				bv.passView.x=(1000-bv.passView.width)/2;
	//			bv.passView.y=470;
				bv.passView.y=440;
				bv.passView.popupCenter=false;
				App.dialog.show(bv.passView);
			}else{
				bv.showInfo("请等待血牡丹操作");
			}
		}
		
		public function passSelect(evt:MouseEvent):void{
			if(evt.currentTarget.name=="yesbtn"){
				selectRole();
			}else{
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,{type:1,tid:GL.tableId,ctype:1,oid:bm.oid});
			}
		}
		
		public function selectRole():void{
			bv.showInfo("请选择要检视的玩家");
			targets=[];
			var arr:Array=phash.values();
			for each(var p:Player in arr){
				if(p.uid != GL.id && !p.isDead && !p.isLost && ((p.rid==29 && p.isSilent==0)?p.black.length<4:p.black.length<3)){
					targets.push(p.view);
				}
			}
			showTarget();
		}
		private var targetid:int=0;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			bv.hideInfo();
			targetid=evt.currentTarget.uid;
			var param:Object={type:1,tid:GL.tableId,ctype:1,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
			clearState();
			targetid=0;
		}
		
		public function delayedFun1():void
		{
			var p:Player=phash.get(tvo.sponsor);
//			var target:Player=phash.get(tvo.target);
			p.view.showTime(10000);
			if(tvo.sponsor==GL.id){
				var temp:Array=[];
				var arr:Array=data.cards;
				for(var i:int in arr){
					var ac:ACard=new ACard();
					ac.setdata(arr[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请检视玩家手牌",0,0);
			}else{
				bv.showInfo("请等待血牡丹检视");
			}
		}
	}
}