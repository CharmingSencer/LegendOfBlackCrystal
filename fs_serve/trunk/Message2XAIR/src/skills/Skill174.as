package skills
{
	import com.greensock.TweenMax;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;

	/**
	 *(新)严刑 
	 */	
	public class Skill174 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			playNormalAni();
//			App.audio.play(Cons.audio.YELLOW);
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
			TweenMax.delayedCall(1.5,delayedFun1);
		}
		public function delayedFun1():void{
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var temp:Array=[];
				for(var i:int in p.hand){
					if(p.hand[i].color>2){
					var c:ACard=new ACard();
					c.setdata(p.hand[i]);
					temp.push(c);
					}
				}
				bv.showCardDialog(temp,"请选择一张黑色手牌",1,2);
			}else{
				bv.showInfo("请等待六姐操作");
			}
		}
	}
}