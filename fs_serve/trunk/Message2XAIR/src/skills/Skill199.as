package skills
{
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	import views.DecodeDialog;

		/**
		 * 绝渡逢舟
		 */	
		public class Skill199 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,6)+".mp3");
			}

		}
}