package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import views.ACard;
	import utils.Rand;

	/**
	 * 桎梏之缚
	 */
	public class Skill188 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			playNormalAni();
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
			TweenMax.delayedCall(1.5,delayedFun);
		}
		
		override public function delayedFun():void
		{
			var arr:Array=phash.values();
			for each(var p:Player in arr){
				if(p.isCommonTarget) p.isLock=true;
			}
		}
	}
	
}