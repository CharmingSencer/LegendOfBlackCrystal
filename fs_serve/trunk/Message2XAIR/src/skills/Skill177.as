package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	import views.DecodeDialog;

		/**
		 * 包裹直达
		 */	
		public class Skill177 extends Skill
		{
			override public function launch():void
			{
				var arr:Array=phash.values();
				targets=[];
				for each(var i:Player in  arr){
//					if(!i.myTurn &&  !i.isDead && !i.isLost && i.uid!=GL.id && !i.isLock && !i.isCaptal){
					if(i.uid!=GL.id && !i.isDead && !i.isLost && (i.isLock || i.isSkip)){
						targets.push(i.view);
					}
				}
				bv.showInfo("请选择一个玩家发动");
				this.showTarget();
			}
			private var targetid:int=-1;
			override protected function onTargetSelect(evt:MouseEvent):void
			{
				targetid=evt.currentTarget.uid;
				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
				clearState();
			}
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				if(obj.goOn){
					delayedFun();
				}else{
					playTurnAni(0,obj.rid);
					App.audio.backPlay(Cons.audio.GAMING_CHRISTMAS);
//					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,6)+".mp3");
					var p:Player=phash.get(tvo.sponsor);
					p.rid=data.rid;
				}
			}
			
			override public function delayedFun():void
			{
				var p:Player=phash.get(tvo.sponsor);
				var t:Player=phash.get(tvo.target);
				p.view.showTime(10000);
				p.view.showChat("嚯嚯嚯嚯，"+t.rname+"，圣诞快乐！收好你的礼物~");
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,6)+".mp3");
				if(p.uid==GL.id){
					var arr:Array=p.infocards;
					var temp:Array=[];
					for(var i:int in arr){
						if(arr[i].color!=3){
							var ac:ACard=new ACard();
							ac.setdata(arr[i]);
							temp.push(ac);
						}
					}
					bv.showCardDialog(temp,"请选择你要转置的情报",1,2);
				}else if(t.uid==GL.id){
					bv.showInfo("圣诞老人正在给你派发礼物！");
				}else{
					bv.showInfo("请等待圣诞老人操作");
				}
			}
		}
}