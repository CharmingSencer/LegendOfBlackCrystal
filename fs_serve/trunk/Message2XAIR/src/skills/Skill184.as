package skills
{
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.OBJUtil;
	import utils.Rand;
	import utils.effect.EffectsManager;
	
	import views.ACard;
	
	/**
	 *重圆
	 */	
	public class Skill184 extends Skill
	{
		override public function launch():void
		{
			var arr:Array=phash.values();
			targets=[];
			for each(var i:Player in  arr){
				if(i.uid!=GL.id && !i.isDead && !i.isLost && int(i.view.hcount.label)>0){
					targets.push(i.view);
				}
			}
			bv.showInfo("请选择一位玩家发动");
			this.showTarget();
		}
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
			clearState();
		}
		
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.goOn){
				excuteBlue();
			}else if(obj.goOn2){
				delayedFun();
			}else if(obj.goOn3){
				delayedFun1();
			}else{
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,2)+".mp3");
//				App.audio.play(Cons.audio.TurnRole);
			}
		}
		
		private var cardvid:int=-1;
//		private var card1vid:int=-1;
//		private var card2vid:int=-1;
//		private var clickCount:int = 0;
		override protected function onCardsSelect(evt:MouseEvent):void
		{
//			clickCount++;
//			var p:Player=phash.get(tvo.sponsor);
//			var t:Player=phash.get(tvo.target);
//			if(t.uid == GL.id){
//				card2vid=evt.currentTarget.vid;
//				clearState();
//			}else if (p.uid==GL.id){
//				card1vid=evt.currentTarget.vid;
				cardvid=evt.currentTarget.vid;
				clearState();
//				App.log.info("1vid1"+card1vid);
//				App.log.info("1vid2"+card2vid);
//			}
//			if(clickCount==2 || sec==0){ //两人都选了，或者时间到了
//			if((card1vid!=-1 && card2vid!=-1)|| sec==0){ //两人都选了，或者时间到了
//				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,card1:card1vid,card2:card2vid,oid:bm.oid};
				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,card:cardvid,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
				clearState();
				cardvid=-1;
//				clickCount = 0;
//			}
		}
		
//		override protected function onCardsSelect1(evt:MouseEvent):void
//		{
//			card2vid=evt.currentTarget.vid;
//			clearState();
//			App.log.info("2vid1"+card1vid);
//			App.log.info("2vid2"+card2vid);
//			if((card1vid!=-1 && card2vid!=-1)|| sec==0){ //两人都选了，或者时间到了
//				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,card1:card1vid,card2:card2vid,oid:bm.oid};
//				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
//				card1vid=-1;
//				card2vid=-2;
//			}
//		}

//		private var sponchoo:Boolean=false;
//		private var targchoo:Boolean=false;
		override public function excuteBlue():void{
			var p:Player=phash.get(tvo.sponsor);
//			var t:Player=phash.get(tvo.target);
			p.view.showTime(10000);
//			t.view.showTime(10000);
//			decrease();
			if(p.uid==GL.id){
				var arr:Array=phash.get(p.uid).hand;
				cards=[];
				for each(var i:ACard in  arr){
					cards.push(i);
				}
				bv.showInfo("请选择一张手牌");
//				sponchoo = true;
				this.showCards();
//			}else if(t.uid == GL.id){
//				var arr2:Array=phash.get(t.uid).hand;
//				cards2=[];
//				for each(var j:ACard in arr2){
//					cards2.push(j);
//				}
//				bv.showInfo("请选择一张手牌");
////				targchoo = true;
//				for(var k:int in cards2){
//					EffectsManager.Glow(cards2[k],0x00ff00,1,4,4,5);
//					cards2[k].addEventListener(MouseEvent.MOUSE_DOWN,onCardsSelect1);
//				}
			}else{
				bv.showInfo("请等待玩家操作");
			}
		}
		
		override public function delayedFun():void{
//			var p:Player=phash.get(tvo.sponsor);
			var t:Player=phash.get(tvo.target);
//			p.view.showTime(10000);
			t.view.showTime(10000);
			if(t.uid == GL.id){
				var arr:Array=phash.get(t.uid).hand;
				cards=[];
				for each(var i:ACard in arr){
					cards.push(i);
				}
				bv.showInfo("请选择一张手牌");
				this.showCards();
			}else{
				bv.showInfo("请等待玩家操作");
			}
		}
		
//		private var sec:int = 10;
//		private function decrease():void{ //倒计时10秒
//			sec-=1;
//			if((card1vid!=-1 && card2vid!=-1)|| sec==0){ //两人都选了，或者时间到了
//				App.log.info("3vid1"+card1vid);
//				App.log.info("3vid2"+card2vid);
//				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,card1:card1vid,card2:card2vid,oid:bm.oid};
//				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
//				card1vid=-1;
//				card2vid=-2;
//			}
//			App.log.info("sec="+sec);
//			if(sec>0){
//				TweenMax.delayedCall(1,decrease);;
//			}
//		}
		
		//将两张展示牌移到屏幕中央,然后回手
		private function delayedFun1():void{
			bv.hideInfo();
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			var arr1:Array=data.card1;
			var arr2:Array=data.card2;
			var temp1:Array = [];
			var temp2:Array = [];
			if(s.uid==GL.id ){
				for(var i:int in arr1){
					temp1.push(s.getHandCardByVid(arr1[i].vid));
					s.removeHandCardByVid(arr1[i].vid);
				}
				for(var j:int in arr2){
					var c:ACard=new ACard();
					c.setdata(arr2[j]);
					temp2.push(c);
				}
			}else if(t.uid==GL.id ){
				for(var k:int in arr2){
					temp2.push(t.getHandCardByVid(arr2[k].vid));
					t.removeHandCardByVid(arr2[k].vid);
				}
				for(var m:int in arr1){
					var c2:ACard=new ACard();
					c2.setdata(arr1[m]);
					temp1.push(c2);
				}
			}else{
				for(var n:int in arr1){
					var c3:ACard=new ACard();
					c3.setdata(arr1[n]);
					temp1.push(c3);
				}
				for(var o:int in arr2){
					var c4:ACard=new ACard();
					c4.setdata(arr2[o]);
					temp2.push(c4);
				}
			}
			bm.sendCardsToCenter(temp1,s);
			TweenMax.delayedCall(1,function():void{
				var centArr:Array=OBJUtil.getCenterPos(Cons.BWID,Cons.BHEI,3,Cons.CWID,Cons.CHEI);
				TweenMax.to(temp1[0],0.2,{x:centArr[0].x,y:centArr[0].y});
				bm.sendCardsToCenter(temp2,t);});
			TweenMax.delayedCall(3,bm.sendCardsToPoint,[temp1,s]);
			TweenMax.delayedCall(4,bm.sendCardsToPoint,[temp2,t]);
			if(data.haveSameColor){
				TweenMax.delayedCall(5,turnRole);
			}
		}
		
		private function turnRole():void{
			var p:Player=phash.get(tvo.sponsor);
			playTurnAni(p.rid,0);
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(3,4)+".mp3");
			p.rid=0;
			if(p.black.length>0){
				TweenMax.delayedCall(1.5,chooseBurn);
			}
		}
		
		private function chooseBurn():void{
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(tvo.dur);
			if(p.uid==GL.id){
				var arr:Array=p.black;
				var temp:Array=[];
				for(var i:int in arr){
					var ac:ACard=new ACard();
					ac.setdata(arr[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择一张黑情报烧毁,不烧毁直接按确定",1,2);
			}else{
				bv.showInfo("请等待镜中人操作");
			}
		}
	}
}