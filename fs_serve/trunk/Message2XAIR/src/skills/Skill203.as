package skills
{
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;
	
	/**
	 *繁华落幕
	 */	
	public class Skill203 extends Skill
	{
		override public function launch():void
		{
			var self:Player=phash.get(GL.id);
			var temp:Array=[];
			for(var i:int in self.hand){
				var ac:ACard=new ACard();
				ac.setdata(self.hand[i]);
				temp.push(ac);
			}
			bv.showCardDialog(temp,"请选择红、蓝、黑色手牌各一张弃置",3,2,-1,onCardselected);
		}
		
		private var selectedCard:Array = [];
		public function onCardselected(selected:Array):void{
			var self:Player=phash.get(GL.id);
			if(selected.length!=3){
				bv.showInfo("未选择足够的手牌，发动失败");
				return;
			}else{
//				var blue:Boolean=false, red:Boolean=false, black:Boolean=false, idle4:Boolean=false, idle5:Boolean=false, valid:Boolean=false;
//				for each(var c:ACard in selected){
//					if(idle4==true && idle5==true) { valid=true; break;}
//					switch(c.color()){
//						case 1:
//							if(red==true && black==true) { valid=true; break;}
//							if(idle5==true){
//								blue=true;
//								black=true;
//							}else blue = true;
//							break;
//						case 2:
//							if(blue==true && black==true) { valid=true; break;}
//							if(idle4==true){
//								red=true;
//								black=true;
//							}else red = true;
//							break;
//						case 3:
//							if(blue==true && red==true) { valid=true; break;}
//							if(idle4==true){
//								red=true;
//								black=true;
//								//							idle4=false; //unnecessary?
//							}if(idle5==true){
//								blue=true;
//								black=true;
//							}else black = true;
//							break;
//						case 4:
//							if(red==true && (black==true || blue==true)) { valid=true; break;}
//							if(idle4==true){
//								red=true;
//								black=true;
//							}else if (idle5==true){
//								idle4=true;
//							}else if(red==true){
//								black=true;
//							}else if(black==true){
//								red=true;
//							}else{
//								idle4=true;
//							}
//							break;
//						case 5:
//							if(blue==true && (black==true || red==true)) { valid=true; break;}
//							if(idle4==true){
//								idle5=true;
//							}else if (idle5==true){
//								blue=true;
//								black=true;
//							}else if(blue==true){
//								black=true;
//							}else if(black==true){
//								blue=true;
//							}else{
//								idle5=true;
//							}
//							break;
//					}
//					if(blue==true && red==true && black==true) { valid=true; break;}
//				}
				var blue:int=0, red:int=0, black:int=0, redblack:int=0, blueblack:int=0, valid:Boolean=false;
				var temp:Array=[];
				for(var j:int in selected){
					for each(var ca:ACard in self.hand){
						if(ca.vid==selected[j]){
							temp.push(ca);
							break;
						}
					}
				}
				for (var c:int in temp){
					switch(temp[c].color){
						case 1:
							blue++; break;
						case 2:
							red++; break;
						case 3:
							black++; break;
						case 4:
							red++; black++; redblack++; break;
						case 5:
							blue++; black++; blueblack++; break;
					}
				}
				if (red>0 && blue>0 && black>0)
					if ((redblack==0 || red + black - 2 > 0) && (blueblack==0 || blue + black - 2 > 0))
						valid = true;
				if(!valid){
					bv.showInfo("选择的手牌不满足条件，发动失败");
					return;
				}
			}
			selectedCard=selected;
			var arr:Array=phash.values();
			targets=[];
			for each(var i:Player in  arr){
				if(!i.isDead && !i.isLost){
					if(i.black.length>0 || i.red.length>0 || i.blue.length>0)
						targets.push(i.view);
				}
			}
			bv.showInfo("请选择一个玩家发动");
			this.showTarget();
		}
		
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			clearState();
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,cards:selectedCard,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
			selectedCard=[];
			targetid=-1;
		}
		
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.goOn){
				excuteBlue();
			}else if (obj.turnRole){
				turnRole();
			}else{
				playNormalAni();
//				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,2)+".mp3");
				var p:Player=phash.get(tvo.sponsor);
				p.rid=data.rid;
			}
		}
		
		override public function excuteBlue():void
		{
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(tvo.dur);
			if(p.uid==GL.id){
				var target:Player=phash.get(tvo.target);
				var arr:Array=target.infocards;
				var temp:Array=[];
				for(var i:int in arr){
					var ac:ACard=new ACard();
					ac.setdata(arr[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择一张情报烧毁",1,2);
			}else{
				bv.showInfo("请等待霓虹操作");
			}
		}
		
		private function turnRole():void
		{
			var p:Player=phash.get(tvo.sponsor);
			playTurnAni(p.rid,0);
			p.rid=data.rid;
		}
	}
}