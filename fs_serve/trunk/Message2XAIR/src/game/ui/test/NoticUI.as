/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class NoticUI extends Dialog {
		public var text1:TextArea = null;
		public var closebtn:Button = null;
		protected static var uiXML:XML =
			<Dialog>
			  <Image skin="png.custom.公告底色" x="0" y="0" width="500" height="570" smoothing="true" sizeGrid="213,100,215,120"/>
			  <TextArea x="34" y="74" width="437" height="452" size="18" color="0xffffff" selectable="true" editable="false" vScrollBarSkin="png.comp.vscroll1" margin="3,0,10,3" var="text1" name="text1" leading="10" underline="false" text="版本更新公告（Version=0.183.0）\n请在进入游戏时更新，谢谢。\n欢迎从群文件下载最新的风声私服手册。\n一、服务器地址更换\n1.服务器地址已更换，旧版本连接的服务器的服务已关闭。\n二、资源更新\n1.添加了方途的部分测试配音。\n\n\n版本更新公告（Version=0.182.0）\n请在进入游戏时更新，谢谢。\n欢迎从群文件下载最新的风声私服手册。\n一、服务器地址更换\n1.服务器地址已更换，旧版本连接的服务器的服务已关闭。\n二、角色调整\n1.税务局长\n-【收税】抽牌数-1。现在可选择是否将一张手牌放回牌库顶。\n2.方途\n-现在，【情报枢纽】指定的目标获得情报时，方途可以抽一张牌。\n-【殊死一搏】不再翻开身份牌，但每回合限发动一次。\n三、bug修复\n1.尝试修复了某些红色技能或红类黄色技能（如【香风毒雾】、【情报枢纽】、【情报诱捕】等）的发动询问按跳过按钮跳不过的问题。（注：此改动涉及的范围很广，请玩家们留意，如有本应能发动的蓝色技能没有询问，请私聊汇报给先生提符，谢谢）。\n\n\n&#xD;"/>
			  <Button skin="png.custom.btn_009gb" x="467" y="16" var="closebtn" name="closebtn"/>
			</Dialog>;
		public function NoticUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}