/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class RoomListItemUI extends View {
		public var lock:Image = null;
		public var mode:Image = null;
		protected static var uiXML:XML =
			<View width="656" height="39">
			  <Image skin="png.custom.s_1" x="327" y="6" name="lock" var="lock"/>
			  <Label text="9999&#xD;" x="58" y="6" width="64" height="25" align="center" bold="false" size="14" font="Microsoft YaHei Bold" name="rid" color="0xcccccc" embedFonts="false"/>
			  <Label text="暗杀者的黑名单" x="128" y="6" width="194" height="25" align="left" bold="true" size="14" name="rname" color="0xcccccc" stroke="0x0" wordWrap="false" multiline="false"/>
			  <Label text="准备中" x="363" y="6" width="61" height="24" align="center" color="0x38b338" bold="true" size="14" stroke="0x0" font="Microsoft YaHei Bold" name="status" embedFonts="false"/>
			  <Label text="200G/Lv.1/50%" x="435" y="5" width="151" height="23" align="center" bold="true" size="14" font="Microsoft YaHei Bold" name="limit" color="0xcccccc"/>
			  <Label text="8/3" x="595" y="5" width="50" height="27" align="center" bold="true" size="14" font="Microsoft YaHei Bold" name="rcount" color="0xcccccc"/>
			  <Image skin="png.custom.mode1_frame" x="6" y="5" var="mode" name="mode"/>
			</View>;
		public function RoomListItemUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}