/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	import game.ui.test.RoomListItemUI;
	public class RoomViewUI extends View {
		public var dev:Box = null;
		public var box1:Box = null;
		public var rlist:List = null;
		public var bt1:Button = null;
		public var bt2:Button = null;
		public var bt3:Button = null;
		public var bt4:Button = null;
		public var closebtn:Image = null;
		public var title1:Image = null;
		public var devbtn:Image = null;
		protected static var uiXML:XML =
			<View width="1000" height="700">
			  <Image skin="jpg.custom.001dtjm" x="0" y="0"/>
			  <Box x="94" y="77" var="dev" name="dev" width="877" height="576" alpha="0">
			    <Image skin="png.rpg.background2" x="0" y="0" width="877" height="603"/>
			    <Image skin="png.rpg.frame0" x="78" y="128"/>
			    <Image skin="png.rpg.frame0" x="78" y="333"/>
			    <Image skin="png.rpg.frame2" x="205" y="128"/>
			    <Image skin="png.rpg.frame2" x="205" y="332"/>
			    <Image skin="png.rpg.frame1" x="329" y="128"/>
			    <Image skin="png.rpg.frame1" x="453" y="128"/>
			    <Image skin="png.rpg.frame1" x="577" y="128"/>
			    <Image skin="png.rpg.frame1" x="701" y="128"/>
			    <Image skin="png.rpg.frame1" x="329" y="332"/>
			    <Image skin="png.rpg.frame1" x="453" y="332"/>
			    <Image skin="png.rpg.frame1" x="577" y="332"/>
			    <Image skin="png.rpg.frame1" x="701" y="332"/>
			  </Box>
			  <List x="33" y="151" width="656" height="394" spaceY="0" spaceX="0" repeatY="10">
			    <Box name="render">
			      <Image skin="png.custom.xmlb_2" x="0" y="0"/>
			    </Box>
			  </List>
			  <Box x="34" y="122" var="box1" name="box1" width="694" height="490">
			    <List x="0" var="rlist" name="rlist" repeatY="9" repeatX="1" y="29" width="679" height="393" spaceY="0">
			      <RoomListItem name="render" y="1" runtime="game.ui.test.RoomListItemUI"/>
			      <VScrollBar skin="png.comp.vscroll1" x="660" width="16" height="392" y="-9" name="scrollBar"/>
			    </List>
			    <Button label="创建房间" skin="png.custom.btn_2" x="0" y="423" stateNum="3" labelColors="0xaaaacc,0xaaaacc,0xaaaacc" labelSize="20" labelBold="true" labelStroke="0x0" labelFont="Microsoft YaHei Bold" var="bt1" name="bt1" width="114" height="44"/>
			    <Button label="显示等待" skin="png.custom.btn_2" x="128" y="423" stateNum="3" labelSize="20" labelBold="true" labelStroke="0" labelFont="Microsoft YaHei Bold" var="bt2" name="bt2" width="114" height="44" labelColors="0xaaaacc,0xaaaacc,0xaaaacc"/>
			    <Button label="加入房间" skin="png.custom.btn_2" x="256" y="423" stateNum="3" labelColors="0xaaaacc,0xaaaacc,0xaaaacc" labelSize="20" labelBold="true" labelStroke="0" labelFont="Microsoft YaHei Bold" width="114" height="44" var="bt3" name="bt3"/>
			    <Button label="快速加入" skin="png.custom.btn_3a" x="531" y="423" stateNum="3" width="114" height="44" labelColors="0xaaaacc,0xaaaacc,0xaaaacc" labelFont="Microsoft YaHei Bold" labelBold="true" labelStroke="0" labelSize="20" var="bt4" name="bt4"/>
			  </Box>
			  <Image skin="png.rpg.closebtn" x="940" y="13" var="closebtn" name="closebtn"/>
			  <Image skin="png.custom.xmlb_1" x="33" y="117" var="title1" name="title1"/>
			  <Image skin="png.custom.t1" x="24" y="23"/>
			  <Image skin="png.rpg.addicon" x="28" y="33" var="devbtn" name="devbtn" visible="false"/>
			</View>;
		public function RoomViewUI(){}
		override protected function createChildren():void {
			viewClassMap["game.ui.test.RoomListItemUI"] = RoomListItemUI;
			super.createChildren();
			createView(uiXML);
		}
	}
}