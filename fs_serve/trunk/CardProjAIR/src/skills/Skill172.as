package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import views.ACard;
	import utils.Rand

	/**
	 * 授业
	 */	
	public class Skill172 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(data.probe){
				var p:Player=phash.get(tvo.target);
				if(p.uid==GL.id){
					for each(var c:ACard in bv.handArea){
						if(c.id==99){
							c.setdata(data.probe);
							c.target=null;
						}
					}
					for each(var ca:ACard in p.hand){
						if(ca.id==99){
							ca.setdata(data.probe);
							ca.target=null;
						}
					}
				}
			}else{
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,5)+".mp3");
				TweenMax.delayedCall(1.5,delayedFun);
			}
		}
		
		override public function delayedFun():void{
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var arr:Array=phash.values();
				targets=[];
				for each(var i:Player in  arr){
					if(!i.isDead && !i.isLost && i.uid!=GL.id){
						targets.push(i.view);
					}
					if(i.uid==GL.id){
						bv.showInfo("请选择一位目标玩家");
					}
				}
				this.showTarget();
			}else{
				bv.showInfo("请等待义书华操作");
			}
		}
		
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
			clearState();
		}
	}
				
}