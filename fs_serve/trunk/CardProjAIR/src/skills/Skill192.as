package skills
{
	import com.greensock.TweenMax;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	import utils.Rand;
	
	import views.ACard;

	/**
	 *黑市交易
	 */	
	public class Skill192 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			playNormalAni();
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,6)+".mp3");
			TweenMax.delayedCall(1.5,delayfun);
		}
		public function delayfun():void{
			var target:Player=phash.get(tvo.target);
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var arr:Array=data.cards;
				var temp:Array=[];
				for(var i:int in arr){
					var ac:ACard=new ACard();
					ac.setdata(arr[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择一张情报转置到你面前",1,2);
			}else{
				bv.showInfo("请等待烈·酒保操作");
			}
		}
	}
}