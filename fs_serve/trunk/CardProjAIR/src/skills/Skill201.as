package skills
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	import datas.TargetVO;
	
	import events.WEvent;
	
	import morn.core.components.Clip;
	
	import util.GUtil;
	
	import utils.ArrayUtil;
	import utils.HashMap;
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	import flash.utils.Timer;
	import flash.events.TimerEvent;

		/**
		 * 华灯初上
		 */	
		public class Skill201 extends Skill
		{
			override public function launch():void
			{
				var self:Player=phash.get(GL.id);
				var temp:Array=[];
				for(var i:int in self.infocards){
					if(self.infocards[i].color!=3){
						var ac:ACard=new ACard();
						ac.setdata(self.infocards[i]);
						temp.push(ac);	
					}
				}
				bv.showCardDialog(temp,"请选择两张红蓝情报",2,2,-1,onCardselected);
			}
			
			private var selectedCard:Array = [];
			public function onCardselected(selected:Array):void{
				if(selected.length!=2){
					bv.showInfo("未选择足够的情报，发动失败");
					return;
				}
				selectedCard=selected;
				clearState();
				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,cards:selectedCard,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
				selectedCard=[];
			}
			
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				if(obj.goOn){
					delayedFun();
				}else{
					
					playTurnAni(0, obj.rid);
//					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+1+".mp3");
					var p:Player=phash.get(tvo.sponsor);
					p.rid=data.rid;
				}
			}
			
			override public function delayedFun():void
			{
				var p:Player=phash.get(tvo.sponsor);
//				p.rid=data.rid;
				var cards:Array=data.cards;
//				var num:int=cards.length;
//				App.log.info("data.numOfShowed="+data.numOfShowed);
				var temp:Array=[];
				for(var i:int in cards){
					var c:ACard=new ACard();
					c.setdata(cards[i]);
					temp.push(c);
				}
				trackCards(temp);
//				var myTimer:Timer=new Timer(1000,temp.length);
//				myTimer.addEventListener(TimerEvent.TIMER, timerhandler);//注意，事件timer必须全部小写
//				myTimer.start();
			}
			
//			private function timerhandler(event:TimerEvent):void{
//				trackCards(temp);
//			}
			
			private var i:int=0;
			public function trackCards(temp:Array):void{ //追踪动画
				i++;
//				for(var i:int=1; i<=temp.length; i++){
					var cpts:Array=OBJUtil.getCenterPos(Cons.BWID,Cons.BHEI,i,Cons.CWID,Cons.CHEI); //cpts: center points
					for(var j:int=0; j<i-1; j++){
						temp[j].x=cpts[j].x;
						temp[j].y=cpts[j].y;
						TweenMax.to(temp[j],0.5,{x:cpts[j].x,y:cpts[j].y});
					}
					temp[i-1].x=cpts[i-1].x;
					temp[i-1].y=cpts[i-1].y;
					TweenNano.from(temp[i-1],0.8,{alpha:0});
					bv.addChild(temp[i-1]);
					if(i<temp.length)TweenMax.delayedCall(1,trackCards,[temp]);
					else TweenMax.delayedCall(1,getCards,[temp]);
//				}
			}
			
			public function getCards(arr:Array):void{ //将展示的追踪到的牌从屏幕中间加入手牌
				i=0;
				var p:Player = phash.get(tvo.sponsor);
				bm.sendCardsToPoint(arr, p);
//				App.audio.play(Cons.audio.Deal);
			}
		}
}