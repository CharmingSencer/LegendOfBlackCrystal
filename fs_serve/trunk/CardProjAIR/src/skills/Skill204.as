package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;

	/**
	 * 密令
	 */	
	public class Skill204 extends Skill
	{
		override public function launch():void
		{
			var arr:Array=phash.get(GL.id).hand;
			cards=[];
			for each(var i:ACard in  arr){
				cards.push(i);
			}
			bv.showInfo("请选择一张手牌");
			this.showCards();
		}
		
		private var cardvid:int=-1;
		override protected function onCardsSelect(evt:MouseEvent):void
		{
			cardvid=evt.currentTarget.vid;
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,card:cardvid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
			clearState();
//			clearState();
//			var arr:Array=phash.values();
//			targets=[];
//			for each(var i:Player in  arr){
//				if(i.uid!=GL.id && !i.isDead && !i.isLost)
//					targets.push(i.view);
//			}
//			bv.showInfo("请选择一个玩家");
////			other=false;
//			this.showTarget();
		}
		override protected function onTargetSelect(evt:MouseEvent):void
		{
//			if(other){
				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:evt.currentTarget.uid,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
				clearState();
//				other=false;
//			}else{
//				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:evt.currentTarget.uid,card:cardvid,oid:bm.oid};
//				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
//				clearState();
//			}
		}

		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.goOn){
				delayedFun();
			}else{
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,5)+".mp3");
			}
		}
		
//		private var other:Boolean=false;
		override public function delayedFun():void
		{
			var p:Player=phash.get(tvo.target);
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var arr:Array=phash.values();
				targets=[];
				for each(var i:Player in  arr){
					if(i.uid!=GL.id && !i.isDead && !i.isLost)
						targets.push(i.view);
				}
				bv.showInfo("请选择直达的传递目标");
//				other=true;
				this.showTarget();
			}else{
				bv.showInfo("请等待"+p.pname+"操作");
			}
			
		}

	}
}