package skills
{
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.ACard;
	import com.greensock.TweenMax;

		/**
		 * 倒影
		 */	
		public class Skill183 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
				TweenMax.delayedCall(1.5,delayedFun);
			}
			
			override public function delayedFun():void
			{
				var p:Player=phash.get(tvo.sponsor);
//				var color:int = data.color;
				p.view.showTime(tvo.dur);
				if(p.uid==GL.id){
					var arr:Array=data.cards;
					var temp:Array=[]
					for(var i:int in arr){
						var ac:ACard=new ACard();
						ac.setdata(arr[i]);
						temp.push(ac);
					}
					bv.showCardDialog(temp,"请选择一张手牌放置",1,2);
				}else{
					bv.showInfo("请等待镜中人操作");
				}
			}
		}
}