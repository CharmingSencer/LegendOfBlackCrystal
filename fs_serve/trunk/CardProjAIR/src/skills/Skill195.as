package skills
{
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	import views.DecodeDialog;

		/**
		 * 脱身一击
		 */	
		public class Skill195 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.ranges(1,2,4,4)+".mp3");
				TweenMax.delayedCall(1.5,roleTurn);
			}
			
			private function roleTurn():void{
				var p:Player=phash.get(tvo.sponsor);
				playTurnAni(p.rid,65);
				if(GL.id==p.uid){
					p.skillHash.clear();
					p.skillHash=null;
					p.clearRoleTip(); //清除角色说明。需要写在set rid函数之前。
					p.rid=65;
					bv.initSkillInfo(p);
				}else{
					p.skillHash=null;
					p.clearRoleTip(); //清除角色说明。需要写在set rid函数之前。
					p.rid=65;
				}
				p.view.hint.visible=false;
			}
		}
}
