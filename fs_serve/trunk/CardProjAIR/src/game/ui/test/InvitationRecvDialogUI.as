/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class InvitationRecvDialogUI extends Dialog {
		public var title:Label = null;
		public var txt:TextArea = null;
		public var bt1:Button = null;
		public var bt2:Button = null;
		public var inviter:TextArea = null;
		protected static var uiXML:XML =
			<Dialog width="634" height="328">
			  <Image skin="png.custom.摧毁底色" x="0" y="0"/>
			  <Label text="邀请函" x="206" y="29" width="229" height="44" color="0xffff66" size="30" align="center" var="title" name="title" font="Microsoft YaHei Bold" bold="true"/>
			  <TextArea text="邀请您一起游戏。是否前往？" x="29" y="168" width="575" height="35" size="25" align="center" color="0xffffff" var="txt" name="txt"/>
			  <Button label="前往" skin="png.custom.btn_2" x="177" y="257" labelColors="0xffffff,0xffffff,0xffffff" labelFont="Microsoft YaHei Bold" labelSize="16" labelBold="true" var="bt1" name="bt1"/>
			  <Button label="算了" skin="png.custom.btn_2" x="344" y="257" labelFont="Microsoft YaHei Bold" labelColors="0xffffff,0xffffff,0xffffff" labelSize="16" labelBold="true" var="bt2" name="bt2"/>
			  <TextArea text="全世界最帅的提符" x="30" y="117" width="573" height="32" size="25" align="center" color="0x33ffff" var="inviter" name="inviter"/>
			</Dialog>;
		public function InvitationRecvDialogUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}