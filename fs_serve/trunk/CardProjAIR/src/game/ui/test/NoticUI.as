/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class NoticUI extends Dialog {
		public var text1:TextArea = null;
		public var closebtn:Button = null;
		protected static var uiXML:XML =
			<Dialog>
			  <Image skin="png.custom.公告底色" x="0" y="0" width="500" height="570" smoothing="true" sizeGrid="213,100,215,120"/>
			  <TextArea x="34" y="74" width="437" height="452" size="18" color="0xffffff" selectable="true" editable="false" vScrollBarSkin="png.comp.vscroll1" margin="3,0,10,3" var="text1" name="text1" leading="10" underline="false" text="版本更新公告（Version=0.184.0）\n请在进入游戏时更新，谢谢。\n欢迎从群文件下载最新的风声私服手册。\n一、新角色\n1.新隐藏角色“虞罂”已加入测试。\n二、角色调整\n1.霓虹\n-酱油任务所集手牌张数由8上调为9。\n2.老金、贝雷帽、破镜\n-酱油任务所集手牌颜色由红和蓝调整为纯红和纯蓝。\n3.破镜\n-【碎裂】现在可以由放置的情报的触发。\n4.血牡丹\n-【岁月留痕】现在可以检视一位其他玩家的手牌。\n5.漩涡\n-【掩饰】现在只能在场上有玩家死亡后发动。\n6.致命香水\n-【临危受命】在3V3模式下不再执行“偷取身份”部分的效果。\n7.黑老大\n-【霸权】现在需要弃2张手牌发动。\n8.刀锋\n-删除技能【敏锐】。\n-新增技能【杀意】：[触发技]当你选择将传递的情报时，你可以将一张黑色手牌当作直达传出。\n三、问题修复\n1.【掩护】转置的情报的来源不再是目标玩家而是钢铁特工（某些酱油任务为亲杀类的角色不再因钢铁特工【掩护】他们的情报自杀而意外获胜了）。\n2.技能名【倍受宠爱】已经更正为【备受宠爱】。\n3.【悲伤颂歌】发动时有了指向目标的箭头动画。\n4.修复了【涌动】的发动会创造一个额外的公共响应阶段的问题。\n5.修复了亲杀系酱油杀死黑老大而本该直接宣胜时黑老大还能发动【殉道】的问题。\n6.修复了酱油场屠城无法宣胜的问题。\n7.修复了被调换过的情报无法触发【去无踪】的问题。\n8.修复了被【幽灵袭击】调换过的情报的来源为诺娃的错误。\n9.修复了【情报枢纽】两段技能的语音放反的错误。修复了该技能部分语音左右声道音量不一的问题。\n10.修复了【光影浮华】能根据手牌顺序靶向指定目标牌的问题。\n11.【枪林弹雨】的颜色已修正为红色。\n\n\n"/>
			  <Button skin="png.custom.btn_009gb" x="467" y="16" var="closebtn" name="closebtn"/>
			</Dialog>;
		public function NoticUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}