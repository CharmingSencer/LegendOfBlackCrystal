package views
{
	import com.adobe.utils.StringUtil;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NativeDragEvent;
	
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	
	import game.ui.test.CreateRoomUI;
	
	import it.gotoandplay.smartfoxserver.SmartFoxClient;
	
	import morn.core.components.HSlider;
	import morn.core.handlers.Handler;
	
	import util.GUtil;
	
	import utils.effect.EffectsManager;
	
	public class CreateRoom extends CreateRoomUI
	{
		public function CreateRoom()
		{
			super();
			hs1.changeHandler=new Handler(onChange,[hs1]);
			hs2.changeHandler=new Handler(onChange,[hs2]);
			hs3.changeHandler=new Handler(onChange,[hs3]);
			bt1.addEventListener(MouseEvent.CLICK,onclick);
			closebtn.addEventListener(MouseEvent.CLICK,onclose);
//			cb1.addEventListener(Event.CHANGE,oncheckboxchange);
			
			//模式选择
			selectedMode = mode1;
			EffectsManager.Glow(selectedMode as Sprite,0xff0000,0.6,6,6,5);
			mode1.addEventListener(MouseEvent.CLICK,onModeUpdate);
			mode2.addEventListener(MouseEvent.CLICK,onModeUpdate);
			mode3.addEventListener(MouseEvent.CLICK,onModeUpdate);
			
			//角色池选择
			selectedPool = pool1;
			EffectsManager.Glow(selectedPool as Sprite,0xff0000,0.6,6,6,5);
			pool1.addEventListener(MouseEvent.CLICK,onPoolUpdate);
			pool2.addEventListener(MouseEvent.CLICK,onPoolUpdate);
		}
		
		protected function oncheckboxchange(event:Event):void
		{
			if(cb1.selected==true){
				hs1.disabled=true;
				hs1.value=0;
				lb1.text="0";
			}else{
				hs1.disabled=false;
				hs1.value=100;
				lb1.text="100";
			}
		}
		
		protected function onclose(event:MouseEvent):void
		{
			this.close();
		}
		
		protected function onclick(event:MouseEvent):void
		{
			if(StringUtil.trim(ti1.text)=="")ti1.text="";
			
			var gameMode:int;
			var rolePool:int;
			var maxU:int;
			
			switch(selectedMode.name){
				case "mode1": 
					gameMode = 1;
					maxU=8;
					break;
				case "mode2":
					gameMode = 2;
					maxU=6;
					break;
				case "mode3":
					gameMode = 3;
					maxU=6;
					break;
				default:
					gameMode = 1;
					break;
			}
			
			switch(selectedPool.name){
				case "pool1": 
					rolePool = 1;
					break;
				case "pool2":
					rolePool = 2;
					break;
				default:
					rolePool = 1;
					break;
			}
			SFS.inst.sfs.sendXtMessage(Cons.extension.roomserv,Cons.cmd.CreateRoom,{maxU:maxU,ctype:0,ante:hs1.value,level:hs2.value,rate:hs3.value,sameip:cb1.selected,pass:ti1.text, gameMode:gameMode, rolePool:rolePool},SmartFoxClient.XTMSG_TYPE_XML);
			this.close();
		}
		
		public function onChange(hs:HSlider,value:int):void{
			switch(hs)
			{
				case hs1:
					 lb1.text=value.toString();
					break;
				case hs2:
					lb2.text="不低于"+value+"级";
					break;
				case hs3:
					lb3.text="不低于"+value+"%";
					break;
			}
		}
		public function refresh(param:Object):void
		{
			if(param){
				
			}else{
				if(GL.coin<0)hs1.max=0;
				else{
					var lvl:int=int(GUtil.getLevelByExp(GL.exp));
//					if(lvl<10)hs1.max=100;
//					else hs1.max=Math.max((lvl-9)*100,1000);
					hs1.max=Math.min(lvl*100,1000);
				}
				hs1.value=100;hs2.value=1;hs3.value=0;ti1.text="";
			}
		}
		
		private var selectedMode:Object;
		protected function onModeUpdate(event:MouseEvent):void
		{
			if(selectedMode)
				EffectsManager.clearGlow(selectedMode as Sprite);
			selectedMode=event.currentTarget;
			EffectsManager.Glow(selectedMode as Sprite,0xff0000,0.6,6,6,5);
			
		}
		
		private var selectedPool:Object;
		protected function onPoolUpdate(event:MouseEvent):void
		{
			if(selectedPool)
				EffectsManager.clearGlow(selectedPool as Sprite);
			selectedPool=event.currentTarget;
			EffectsManager.Glow(selectedPool as Sprite,0xff0000,0.6,6,6,5);
			
		}
	}
}