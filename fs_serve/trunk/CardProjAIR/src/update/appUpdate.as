package update
{
		
		import air.update.ApplicationUpdaterUI;
		
		import air.update.events.StatusUpdateEvent;
		
		import air.update.events.UpdateEvent;
		
		
		
		import flash.desktop.NativeApplication;
		
		import flash.display.NativeWindow;
		
		import flash.display.Sprite;
		
		import flash.events.ErrorEvent;
		
		
		
		/**
		 
		 * 应用更新升级检查类
		 
		 */ 
		
		public class appUpdate
			
		{
			
			
			
			private var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();
			
			
			
			public function appUpdate()
				
			{
				
				
				
			}
			
			
			
			/**
			 
			 * 检查是否有更新
			 
			 */ 
			
			public function checkUpdate():void {
				
				
//				appUpdater.updateURL = "http://127.0.0.1:8080/update.xml";
				appUpdater.updateURL = "https://gitlab.com/CharmingSencer/LegendOfBlackCrystal/raw/master/updateDescriptor.xml";
//				appUpdater.updateURL = "http://47.52.143.39:8081/fsupdate/updateDescriptor.xml";
				
				appUpdater.addEventListener(UpdateEvent.INITIALIZED, onUpdate);
				
				appUpdater.addEventListener(ErrorEvent.ERROR, onError);
				
				appUpdater.isCheckForUpdateVisible = false;//“检查更新”对话框不可见
				
				appUpdater.isFileUpdateVisible = false;//“文件更新”对话框不可见
				
				appUpdater.isDownloadProgressVisible = true;//“下载进度”对话框不可见
				
				appUpdater.isDownloadUpdateVisible = true;//“下载更新”对话框不可见
				
				appUpdater.isInstallUpdateVisible = true;//“安装更新”对话框不可见
				
				appUpdater.initialize();
				
				
				
			}
			
			
			
			
			
			private function onUpdate(event:UpdateEvent):void {
				
				
				
				appUpdater.checkNow();
				
			}
			
			
			
			private function onError(event:ErrorEvent):void {
				
				trace(event.toString());
				
			}
			
		}

}
