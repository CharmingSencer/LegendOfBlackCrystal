/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class NoticUI extends Dialog {
		public var text1:TextArea = null;
		public var closebtn:Button = null;
		protected static var uiXML:XML =
			<Dialog>
			  <Image skin="png.custom.公告底色" x="0" y="0" width="500" height="570" smoothing="true" sizeGrid="213,100,215,120"/>
			  <TextArea x="34" y="74" width="437" height="452" size="18" color="0xffffff" selectable="true" editable="false" vScrollBarSkin="png.comp.vscroll1" margin="3,0,10,3" var="text1" name="text1" leading="10" underline="false" text="版本更新公告（Version=0.171.0）\n请在进入游戏时更新，谢谢。\n最新的风声私服手册为0.167版本，欢迎从群文件下载。\n***更新内容：\n一、客户端\n1.更换了黄雀、闪灵的插画。\n2.为烈添加了配音进行测试。感谢SIYIMOO的配音。欢迎玩家对配音提出建议和意见。\n3.现在，玩家断线重连后会自动进入托管状态，必须手动解除。这项改动是为了解决重连失败的玩家会一直读条，影响其他玩家游戏体验的问题。\n4.现在，一位玩家死亡后，背景音乐将切换至“战斗中-紧张”。\n5.圣诞老人禁选。\n6.年开放选择。\n二、bug修复\n1.修复了断线重连后，重连的玩家看不见重连前已在情报区的卡牌的移动（比如被烧毁、被移动到手牌区等等）动画的bug。\n2.修复了断线重连后，大厅的聊天区还显示的bug。\n3.现在，断线重连后，盖伏着的角色的性别将不再能从语音判断。就算重连前该角色已经翻开过，重连后也将重置为未知（男声播报）。\n4.现在，断线重连后，将播放正确的背景音乐。\n4.修复了一个bug，该bug曾导致客户端从第二局游戏开始，每当有卡牌进入弃牌堆时，客户端都会报错。\n5.修复了一个bug，该bug曾导致选择无选择语音的角色时，客户端都会报错。\n三、已知bug\n1.如果玩家进行过断线重连，在开始下一局游戏前，需要关闭客户端重新登录，否则会出现错误。\n\n\n"/>
			  <Button skin="png.custom.btn_009gb" x="467" y="16" var="closebtn" name="closebtn"/>
			</Dialog>;
		public function NoticUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}