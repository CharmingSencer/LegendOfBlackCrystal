package skills
{
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	import views.DecodeDialog;

		/**
		 * 高屋建瓴
		 */	
		public class Skill186 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				if(obj.goOn){
					excuteBlue();
				}else if(obj.goOn2){
					end();
				}else{
					playNormalAni();
					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
				}
			}

			override public function excuteBlue():void
			{
				var cards:Array=data.cards;
				var temp:Array=[];
				for(var i:int in cards){
					var c:ACard=new ACard();
					c.setdata(cards[i]);
					temp.push(c);
				}
				bm.addCardsToCenter(temp,true);//展示牌3秒
				TweenMax.delayedCall(3,function(arr:Array):void{
					for  (var j:int in arr) {
						bv.removeChild(arr[j]);
					}
					delayedFun();
				},[temp]);
			}
			
			override public function delayedFun():void{
				var p:Player=phash.get(tvo.sponsor);
				p.view.showTime(tvo.dur);
				if(p.uid==GL.id){
					var arr:Array=data.cards2;
					var temp:Array=[]
					for(var i:int in arr){
						var ac:ACard=new ACard();
						ac.setdata(arr[i]);
						temp.push(ac);
					}
					bv.showCardDialog(temp,"请选择一张牌",1,2,-1,onCardselected);
				}else{
					bv.showInfo("请等待墨鸦操作");
				}
			}
			
			private var selectedCard:Array = [];
			public function onCardselected(selected:Array):void{
				selectedCard=selected;
				var arr:Array=phash.values();
				targets=[];
				for each(var i:Player in  arr){
					if(!i.isDead && !i.isLost){
						targets.push(i.view);
					}
				}
				bv.showInfo("请选择一个玩家放置");
				this.showTarget();
			}
			
			private var targetid:int=-1;
			override protected function onTargetSelect(evt:MouseEvent):void
			{
				bv.hideInfo();
				targetid=evt.currentTarget.uid;
				var param:Object={type:1,tid:GL.tableId,ctype:1,target:targetid,cards:selectedCard,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
				clearState();
			}
			
			public function end():void{
				var target:Player=phash.get(tvo.target);
				var cards:Array=data.cards;
				var card:Array=data.cards2;
				var temp:Array=[];
				for(var i:int in cards){
					var c:ACard=new ACard();
					c.setdata(cards[i]);
					temp.push(c);
				}
				var temp2:Array=[];
				for(var j:int in temp){
					if(temp[j].vid==card[0].vid){
						temp2.push(temp[j]);	
					}
				}
				bm.addCardsToCenter(temp,true);
				TweenMax.delayedCall(1,bm.sendAllCardsToInfo,[temp2,target]);
				TweenMax.delayedCall(2,function(arr:Array, arr2:Array):void{
					for  (var j:int in arr) {
						if(arr[j].vid != arr2[0].vid){
							bv.removeChild(arr[j]);
						}
					}
				},[temp,temp2]);
				
			}
		}
}