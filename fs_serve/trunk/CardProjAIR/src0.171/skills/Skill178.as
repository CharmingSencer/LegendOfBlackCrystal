package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Evt;
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import handlers.BattleHandler;
	
	import utils.Rand;
	import utils.effect.EffectsManager;
	
	import views.ACard;

	/**
	 * 礼物派送
	 */	
	public class Skill178 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.handOutToFrom){
				delayedFun();
			}else if(obj.handOutToTo){
				delayedFun1();
			}else if(obj.turnRole){
				turnRole();
			}else{
				playNormalAni();
//				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,6)+".mp3");
			}
		}
		
		override public function delayedFun():void
		{
			var p:Player=phash.get(tvo.sponsor);
			App.log.info(p.view.pname.text==""?p.view.htxt.text:p.view.pname.text);
			var from:Player=phash.get(data.from);
			p.view.showChat("嚯嚯嚯嚯，来来来，收好！");
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,2)+".mp3");
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var temp:Array=[];
				for(var i:int in p.hand){
					var ac:ACard=new ACard();
					ac.setdata(p.hand[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择1张手牌交给"+(from.view.pname.text==""?from.view.htxt.text:from.view.pname.text),1,2,from.uid);
			}else{
				bv.showInfo("请等待圣诞老人选择礼物派送");
			}
		}
		
		public function delayedFun1():void
		{
			var p:Player=phash.get(tvo.sponsor);
			var to:Player=phash.get(data.to);
			p.view.showChat("嚯嚯嚯嚯，别急，每个人都有份！");
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(3,4)+".mp3");
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var temp:Array=[];
				for(var i:int in p.hand){
					var ac:ACard=new ACard();
					ac.setdata(p.hand[i]);
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择1张手牌交给"+(to.view.pname.text==""?to.view.htxt.text:to.view.pname.text),1,2,to.uid);
			}else{
				bv.showInfo("请等待圣诞老人选择礼物派送");
			}
		}
		
		private function turnRole():void{
			var p:Player=phash.get(tvo.sponsor);
			playTurnAni(p.rid,0);
//			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(6,6)+".mp3");
			p.rid=0;
		}
	}
}