package skills
{
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	import datas.TargetVO;
	
	import events.WEvent;
	
	import morn.core.components.Clip;
	
	import util.GUtil;
	
	import utils.ArrayUtil;
	import utils.HashMap;
	import utils.OBJUtil;
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;

		/**
		 * 破裂
		 */	
		public class Skill182 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				if(obj.hasRB){
					delayedFun();
				}else{
					playTurnAni(0,obj.rid);
					App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
					App.audio.play(Cons.audio.TurnRole);
					var p:Player=phash.get(tvo.sponsor);
					p.rid=data.rid;	
				}
				
			}
			
			override public function delayedFun():void
			{
				var p:Player=phash.get(tvo.sponsor);
				p.view.showTime(tvo.dur);
				if(p.uid==GL.id){
					var target:Player=phash.get(tvo.target);
					var arr:Array=target.infocards;
					var temp:Array=[];
					for(var i:int in arr){
						if(arr[i].color != 3){
							var ac:ACard=new ACard();
							ac.setdata(arr[i]);
							temp.push(ac);
						}
					}
					bv.showCardDialog(temp,"请选择一张情报发动",1,2);
				}else{
					bv.showInfo("请等待镜中人操作");
				}
			}
			
			
		}
}