package skills
{
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.OBJUtil;
	import utils.Rand;
	import utils.effect.EffectsManager;
	
	import views.ACard;
	import views.DecodeDialog;
	
	/**
	 *胁迫
	 */	
	public class Skill190 extends Skill
	{
		override public function launch():void
		{
			var arr:Array=phash.values();
			targets=[];
			for each(var i:Player in  arr){
				if(i.uid!=GL.id && !i.isDead && !i.isLost && int(i.view.hcount.label)>0){
					targets.push(i.view);
				}
			}
			bv.showInfo("请选择一位玩家发动");
			this.showTarget();
		}
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
			clearState();
		}
		
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.goOn){
				excuteBlue();
			}else if(obj.goOn2){
				delayedFun();
			}else if(obj.discard){
				chooseDiscard();
			}else if(obj.placeBlack){
				placeBlack();
			}else{
				var p:Player=phash.get(tvo.sponsor);
				p.rid=data.rid;
				playTurnAni(0,p.rid);
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,5)+".mp3");
				App.audio.play(Cons.audio.TurnRole);
			}
		}
		
		override public function excuteBlue():void{
			var p:Player=phash.get(tvo.sponsor);
			var t:Player=phash.get(tvo.target);
			p.view.showTime(10000);
			if(p.uid==GL.id){
				clearState();
				if(!bv.decodedialog){
					bv.decodedialog=new DecodeDialog();
					bv.decodedialog.popupCenter=true;
				}
				bv.decodedialog.callFun=onColorChoosed;
				bv.decodedialog.popup();
			}else{
				bv.showInfo("请等待审讯官操作");
			}
		}
		public function onColorChoosed(color:int):void{
			var param:Object={sponsor:GL.id,tid:GL.tableId,ctype:1,type:color,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
			clearState();
		}
		
		override public function delayedFun():void{
			var p:Player=phash.get(tvo.sponsor);
			var t:Player=phash.get(tvo.target);
//			p.view.showTime(10000);
			t.view.showTime(10000);
			var color:int = data.color as int;
			var quote:String;
			switch(color){
				case 1:
					quote = "给我蓝情报！";
					break;
				case 2:
					quote = "把红情报交出来！";
					break;
				case 3:
					quote = "我知道你有黑情报！";
					break;
				default:
					quote= "我知道你有黑情报！";
					break;
			}
			p.view.showChat(quote);
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range((10+color),(10+color))+".mp3");
			if(t.uid == GL.id){
				var arr:Array=t.hand;
				var temp:Array=[];
				for(var i:int in arr){
					if(color<3){
						if(arr[i].color == color || arr[i].color == 6-color){
							var ac:ACard=new ACard();
							ac.setdata(arr[i]);
							temp.push(ac);
						}
					}else{
						if(arr[i].color >= color){
							var ac:ACard=new ACard();
							ac.setdata(arr[i]);
							temp.push(ac);
						}
					}
				}
				bv.showCardDialog(temp,"请选择",1,72);
			}else{
				bv.showInfo("请等待玩家操作");
			}
		}

		private function chooseDiscard():void{
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(21,27)+".mp3");
			var t:Player=phash.get(tvo.target);
			var p:Player=phash.get(tvo.sponsor);
			p.view.showTime(9000);
			if(p.uid==GL.id){
				var num:int=data.num;
				var temp:Array=[]
				for(var i:int=0;i<num;i++){
					var ac:ACard=new ACard();
					ac.bg.bitmapData=new Act0;
					temp.push(ac);
				}
				bv.showCardDialog(temp,"请选择一张牌弃置,不弃置直接按确定",1,1);
			}else{
				bv.showInfo("请等待审讯官操作");
			}
		}
		
		private function placeBlack():void{
			var p:Player=phash.get(tvo.sponsor);
			var t:Player=phash.get(tvo.target);
			p.view.showTime(10000);
			if(p.uid == GL.id){
				var arr:Array=p.hand;
				var temp:Array=[];
				for(var i:int in arr){
					if(arr[i].color >= 3){
						var ac:ACard=new ACard();
						ac.setdata(arr[i]);
						temp.push(ac);
					}
				}
				bv.showCardDialog(temp,"请选择一张手牌放置到目标面前，不放置直接按确定",1,2);
			}else{
				bv.showInfo("请等待审讯官操作");
			}
		}
	}
}