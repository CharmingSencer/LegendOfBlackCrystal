package views
{
	import flash.events.MouseEvent;
	
	import core.mng.Evt;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	
	import events.WEvent;
	
	import game.ui.test.InvitationRecvDialogUI;
	
	import it.gotoandplay.smartfoxserver.SmartFoxClient;
	
	import util.GUtil;
	import utils.StringUtils;
	
	public class InvitationRecvDialog extends InvitationRecvDialogUI
	{
		public function InvitationRecvDialog()
		{
			super();
			bt1.addEventListener(MouseEvent.CLICK,onBtnclick);
			bt2.addEventListener(MouseEvent.CLICK,onBtnclick);
		}
		
		private var passInput:PassInput;
		protected function onBtnclick(event:MouseEvent):void
		{
			if(event.currentTarget.name=="bt1")
			{
				if(isLock){
					if(!passInput){
						passInput=new PassInput();
						passInput.popupCenter=true;
					}
					passInput.refresh(this.tid);
					App.dialog.popup(passInput);
				}else{
					SFS.inst.sfs.sendXtMessage(Cons.extension.roomserv,Cons.cmd.JoinRoom,{id:this.tid,ctype:0,pass:""},SmartFoxClient.XTMSG_TYPE_XML);
				}
			}else{
//				Evt.dipatch(WEvent.ON_INBATTLECONFIRM,{type:1});
			}
			this.close();
		}
		
		public function initInfo(type:int):void{
			switch(type)
			{
				case 1:
					txt.text="";
					bt1.label="前往";
					bt2.label="算了";
					break;
			}
		}
		
		private var tid:int;
		private var isLock:Boolean;
		public function refresh(param:Object):void
		{
			if(param){
//				var tid:int = param.tid;
				this.tid = param.tid;
				var inviterid:int = param.inviterid;
				var nickname:String = param.inviterNickName;
				this.inviter.text = nickname;
				this.txt.text="邀请您一起游戏，是否前往？";
			}else{
//				hs1.max=int(GUtil.getLevelByExp(GL.exp))*100;
//				hs1.value=100;hs2.value=1;hs3.value=0;ti1.text="";
			}
		}
	}
}