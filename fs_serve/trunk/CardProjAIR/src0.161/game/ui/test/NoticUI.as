/**Created by the Morn,do not modify.*/
package game.ui.test {
	import morn.core.components.*;
	public class NoticUI extends Dialog {
		public var text1:TextArea = null;
		public var closebtn:Button = null;
		protected static var uiXML:XML =
			<Dialog>
			  <Image skin="png.custom.公告底色" x="0" y="0" width="500" height="570" smoothing="true" sizeGrid="213,100,215,120"/>
			  <TextArea x="34" y="74" width="437" height="452" size="18" color="0xffffff" selectable="true" editable="false" vScrollBarSkin="png.comp.vscroll1" margin="3,0,10,3" var="text1" name="text1" leading="10" underline="false" text="版本更新公告（Version=0.161.0）\n新的客户端文件②CardProjAIR0.161.air已经上传到群文件，在进入游戏前请务必更新，谢谢。\n风声私服手册0.110版本已经上传到群文件，欢迎下载。\n***更新内容：\n一、服务器\n- 【【【【重要】】】】IP地址已更换，玩家们请下载安装最新版客户端，否则无法进入游戏。\n二、新角色\n1.新公开角色“黛莎”已加入游戏，具体技能请前往图鉴查看。\n三、角色调整\n1.墨鸦\n- 移除了【险中求胜】只能在自己回合内发动的限制。\n- 【险中求胜】现在可以指定没有情报的角色。\n- 【高屋建瓴】现在只能在自己回合内发动。\n- 酱油任务调整为“一位已获红蓝情报不多于二张的其他玩家因你的情报而死亡”。\n四、bug修复\n1.尝试修复了酱油蝮蛇不夺取其他酱油的宣胜而是与之共同胜利的bug。\n2.修复了雨传出的情报致死后发动【顺水推舟】，展示牌与情报颜色完全相同时，会播放雨盖伏动画的bug。\n3.修复了一个bug，该bug曾导致出牌阶段1当前回合玩家的【增援】被响应导致该玩家死亡后，仍要求该玩家选择将要传递的情报，若不选增援得到的牌则会卡死。\n4.修复了出牌阶段1当前回合玩家被杀死，该玩家在结算死亡后仍结算失败的bug。\n"/>
			  <Button skin="png.custom.btn_009gb" x="467" y="16" var="closebtn" name="closebtn"/>
			</Dialog>;
		public function NoticUI(){}
		override protected function createChildren():void {
			super.createChildren();
			createView(uiXML);
		}
	}
}