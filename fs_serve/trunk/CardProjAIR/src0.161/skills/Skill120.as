package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import morn.core.components.Clip;
	
	import utils.ArrayUtil;
	import utils.HashMap;
	import utils.Rand;
	
	import views.ACard;
	import morn.core.components.List;

		/**
		 * 窃听
		 */	
		public class Skill120 extends Skill
		{
			override public function launch():void
			{
				var arr:Array=phash.values();
				targets=[];
				for each(var i:Player in  arr){
					if(i.uid!=GL.id && i.isDead==false && i.isLost==false && int(i.view.hcount.label)>0){
						targets.push(i.view);
					}
					if(i.uid==GL.id){
						bv.showInfo("请选择最多2个目标并点击确定发动");
						bv.showSelectTarget("请选择完目标点击确定发动",onSelected);
					}
				}
				this.showTarget();
			}
			public function onSelected(str:String):void{
				var temp:Array=[];
				var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,oid:bm.oid};
				if(dict && dict.size()>0){
					var keys:Array=dict.keys();
					for each(var o:Object in keys){
						temp.push(o.uid);
					}
					param.targets=temp;	
				}else{
					param.targets=null;	
				}
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
				clearState();
			}
			
			private var dict:HashMap;
			private var nums:Array;
			override protected function onTargetSelect(evt:MouseEvent):void
			{
				if(!dict)dict=new HashMap();
				var obj:Object=evt.currentTarget;
				if(dict.get(obj)){ //再次点击时，取消已选目标
					bv.removeChild(dict.get(obj));
					ArrayUtil.removeItem(nums,dict.get(obj));
					dict.get(obj).dispose();
					dict.remove(obj);
					flushClips();
				}else{ //用数字标记选择的目标
					if(!nums)nums=[];
					//2018.4.13 0.157 begins
					if(dict.size()==2){
						var keys:Array=dict.keys();
						for each(var o:Object in keys){
							if(dict.get(o)==nums[0]){
								nums.shift(); //删除并返回数组的第一个元素
								dict.get(o).dispose();
								dict.remove(o);
							}
						}
					}
					//2018.4.13 0.157 ends
					var clip:Clip=new Clip();
					clip.autoPlay=false;
					clip.skin="png.comp.clip_num";
					clip.clipX=10;
					clip.x=obj.x+obj.width/2-clip.width;
					clip.y=obj.y+obj.height/2-clip.height;
					bv.addChild(clip);		
					nums.push(clip);
					dict.put(obj,clip);
					flushClips();
				}
			}
			private function flushClips():void{
				var index:int=0;
				if(nums){
					for each(var o:Clip in nums){
						o.index=index++;
					}
				}
			}
			override public function clearState():void
			{
				super.clearState();
				nums=null;
				if(dict){
					var arr:Array=dict.values();
					for(var i in arr){
						bv.removeChild(arr[i] as Clip);	
					}
					dict.clear();
					dict=null;
				}
			}
			
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,3)+".mp3");
				// 2018.4.13 0.157 begins
				var sponsor:Player=phash.get(tvo.sponsor);
//				for each(var i:int in tvo.targets){
//					bm.showArrow(sponsor,phash.get(i));
//				}
				if(tvo.targets.length>0){
					if(tvo.targets.length>=1){
						bm.showArrow(sponsor,phash.get(tvo.targets[0]));
						if(tvo.targets.length>=2){
							TweenMax.delayedCall(1,function():void{bm.showArrow(sponsor,phash.get(tvo.targets[1]));});
						}
					}
				}
				bm.clearState(true);
				// 2018.4.13 0.157 ends
			}
			
			
			/* Old 窃听
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playYellowAni();
				App.audio.play(Cons.audio.YELLOW);
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,3)+".mp3");
				selectPlayers=[];
				TweenMax.delayedCall(3,delayedFun1);
			}
			public function delayedFun1():void{
				var p:Player=phash.get(tvo.sponsor);
				if(selectPlayers.length==0)
					p.view.showTime(10000);
				if(p.uid==GL.id){
					var arr:Array=phash.values();
					targets=[];
					for each(var i:Player in  arr){
						if(i.uid!=GL.id  &&  !i.isDead && !i.isLost && int(i.view.hcount.label)>0){
							if(selectPlayers.length==0 || selectPlayers[0]!=i.uid)
								targets.push(i.view);
						}
					}
					bv.showInfo("请选择第"+(selectPlayers.length+1)+"位玩家");
					this.showTarget();
				}else{
					bv.showInfo("请等待肖惜灵操作");
				}
			}
			private var targetid:int=-1;
			private var selectPlayers:Array;
			override protected function onTargetSelect(evt:MouseEvent):void
			{
				selectPlayers.push(evt.currentTarget.uid);
				clearState();
				var arr:Array=phash.values();
				var h:int;//有手牌的其他玩家的数量
				for each(var i:Player in arr){
					if(i.uid!=GL.id && int(i.view.hcount.label)>0){
						h++;
					}
				}
				if(h>=2 && selectPlayers.length<2){
					delayedFun1();
					return;
				}
				var param:Object={sponsor:GL.id,tid:GL.tableId,ctype:1,targets:selectPlayers,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
			}
			Old 窃听*/
		}
}