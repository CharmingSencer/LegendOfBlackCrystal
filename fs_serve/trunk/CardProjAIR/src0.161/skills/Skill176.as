package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Evt;
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import handlers.BattleHandler;
	
	import utils.Rand;
	import utils.StringUtils;
	import utils.effect.EffectsManager;
	
	import views.ACard;

	/**
	 * 顺水推舟
	 */	
	public class Skill176 extends Skill
	{
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			playNormalAni();
			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,3)+".mp3");
			TweenMax.delayedCall(2,delayedFun);
		}
		
		override public function delayedFun():void//展示牌库顶的第一张牌
		{
			var cards:Array=data.cards;
			var temp:Array=[];
			for(var i:int in cards){
				var c:ACard=new ACard();
				c.setdata(cards[i]);
				temp.push(c);
			}
//			var p:Player=phash.get(tvo.sponsor);
//			p.view.showTime(tvo.dur);
			var target:Player=phash.get(tvo.target);
			bm.addCardsToCenter(temp,true);
			if(data.isSameColor){
				if(data.targetDied){
					TweenMax.delayedCall(1.5,removeCards,[temp]); //调用“移除展示牌”方法
				}else{
					TweenMax.delayedCall(1.5,bm.sendAllCardsToInfo,[temp,target]); //将牌放置到目标面前
				}
			}else{
//				TweenMax.delayedCall(1.5,drawCard,[temp]); //调用“获得展示牌”方法 //获得展示牌版本
				TweenMax.delayedCall(1.5,removeCards,[temp]); //调用“移除展示牌”方法 //不获得展示牌版本
			}
		}
		
		public function removeCards(arr:Array):void{//移除展示牌
			for  (var j:int in arr) 
			{
				bv.removeChild(arr[j]);
			}
			if(data.targetDied==false) turnRole(); //不获得展示牌版本
		}
				
		public function drawCard(arr:Array):void{//获得展示牌
			var p:Player = phash.get(tvo.sponsor);
			bv.setReport(StringUtils.getColorString("["+(p.view.pname.text==""?p.view.htxt.text:p.view.pname.text)+"]",0xffff00)+"获得了"+arr.length+"张展示牌");
			bm.sendCardsToPoint(arr, p);
			App.audio.play(Cons.audio.Deal);
			TweenMax.delayedCall(1,turnRole);
		}
		
		private function turnRole():void{ //盖伏角色牌
			var p:Player=phash.get(tvo.sponsor);
			playTurnAni(p.rid,0);
			//			App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,2)+".mp3");
			p.rid=0;
		}
	}
}