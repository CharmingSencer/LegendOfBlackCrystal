package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	
	import views.DecodeDialog;

		/**
		 * 凛然
		 */	
		public class Skill173 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
				TweenMax.delayedCall(1.5,delayedFun);
			}
			
			override public function delayedFun():void{
				var sponsor:Player=phash.get(tvo.sponsor);
				var target:Player=phash.get(tvo.target);
				sponsor.view.showTime(10000);
				if(sponsor.uid==GL.id){
					clearState();
					var decodedialog:DecodeDialog = new DecodeDialog();
					decodedialog.popupCenter=true;
					//							decodedialog.bt3.visible=false;
					//					bv.decodedialog.bt3.disabled=true;
					decodedialog.title.text="杀你的凶手是"+(target.view.pname.text==""?target.view.htxt.text:target.view.pname.text)+"，你选择：";
					decodedialog.bt1.label="沉默之";
					decodedialog.bt1.labelFont="Microsoft YaHei Bold"
					decodedialog.bt1.labelColors="0x000000";
					decodedialog.bt2.label="弃光其手牌";
					decodedialog.bt2.labelFont="Microsoft YaHei Bold"
					decodedialog.bt2.labelColors="0x000000";
					decodedialog.bt3.label="放弃";
					decodedialog.bt3.labelFont="Microsoft YaHei Bold"
					decodedialog.bt3.labelColors="0x000000";
					//							if(int(sponsor.view.hcount.label) < 1) decodedialog.bt2.disabled = true; //如果没手牌，就不能选择弃光手牌
					decodedialog.callFun=onTypeChoosed;
					decodedialog.popup();
				}else{
					bv.showInfo("请等待义书华操作");
				}
			}
			
			private var type:int =0;
			public function onTypeChoosed(type:int):void{//1沉默 2弃牌 3取消
				this.type=type;
				var param:Object={sponsor:GL.id,tid:GL.tableId,ctype:1,type:type,oid:bm.oid};
				SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnChooseRecieve,param);
				clearState();
				//				}
			}
		}
}