package skills
{
	import com.greensock.TweenMax;
	
	import flash.events.MouseEvent;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	import utils.StringUtils;
	
	import views.ACard;
	
	/**
	 * 猛兽突袭
	 */	
	public class Skill179 extends Skill
	{
		override public function launch():void
		{
			var arr:Array=phash.values();
			targets=[];
			for each(var i:Player in  arr){
				if(!i.isDead && !i.isLost){
					if(i.uid != GL.id)
						targets.push(i.view);
				}
			}
			bv.showInfo("请选择一个玩家发动");
			this.showTarget();
		}
		
		private var targetid:int=-1;
		override protected function onTargetSelect(evt:MouseEvent):void
		{
			targetid=evt.currentTarget.uid;
			var param:Object={sponsor:GL.id,tid:GL.tableId,sid:id,ctype:1,target:targetid,oid:bm.oid};
			SFS.sendWith(Cons.extension.roomserv,Cons.cmd.OnSkillLaunch,param);
			clearState();
		}
		
		override public function play(tvo1:SkillVO,obj:Object):void
		{
			super.play(tvo1,obj);
			if(obj.phase2){
				delayedFun2();
			}else if(obj.phase3){
				placeFirecrackers();
			}else{
				playNormalAni();
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
				TweenMax.delayedCall(1,delayedFun1);
			}
		}
		public function delayedFun1():void{
			var p:Player=phash.get(tvo.sponsor);
			bv.hideInfo();//关掉中间的信息显示，不然会挡住牌的显示
			p.view.showTime(10000);
			if(p.uid==GL.id){
				var x:int = data.x;
//				var cards:Array=data.cards;
				var temp:Array=[];

				for(var i:int = 0; i < x+1; ++i){ //在即将弹出的对话框中显示X+1张爆竹供玩家选择
					var c:ACard=new ACard();
					//				c.setdata(cards[0]);
					c.vid=999;
					c.color=3; //set color before setting id.
					c.id=23;
					c.send=2;
//					temp.push(c.clone);
					temp.push(c);
				}
				p.view.showTime(tvo.dur);
				App.log.info("temp.length is " + temp.length);
				bv.showCardDialog(temp,"请选择要抽并交给目标玩家的【爆竹】",x+1,2);
				
			}else{
				bv.showInfo("请等待年操作");
			}
		}
		
		//将爆竹交给目标
		public function delayedFun2():void{			
			//bv.hideInfo();//关掉中间的信息显示，不然会挡住牌的显示
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			var arr:Array=data.fireCrackers;
			var dilivered:Array=[];
			bv.setReport(StringUtils.getColorString("["+(s.view.pname.text==""?s.view.htxt.text:s.view.pname.text)+"]",0xffff00)+"" + "抽取了"
				+arr.length+"张【爆竹】并交给了" +StringUtils.getColorString( "["+(t.view.pname.text==""?t.view.htxt.text:t.view.pname.text)+"]",0xffff00));
			for(var i:int in arr){
				var c:ACard=new ACard();
				c.setdata(arr[i]);
				dilivered.push(c);
			}
			bm.addCardsToCenter(dilivered,true);
			TweenMax.delayedCall(1.5,function():void{
				bm.sendCardsToPoint(dilivered, t);
//				App.audio.play(Cons.audio.Deal);
				TweenMax.delayedCall(1,delayedFun3);});
		}
		
		//随机抽取目标等量于交出爆竹的手牌
		private function delayedFun3():void{
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			var arr:Array=data.ramPickedHandCards;
			var temp:Array=[];
			if(t.uid==GL.id ){
				for(var i:int in arr){
					temp.push(t.getHandCardByVid(arr[i].vid));
					t.removeHandCardByVid(arr[i].vid);
				}
			}else{
				for(var i:int in arr){
					var c:ACard=new ACard();
					c.setdata(arr[i]);
					temp.push(c);
				}
			}
			bm.sendCardsToCenter(temp,t);
			TweenMax.delayedCall(1.5,delayedFun4,[temp]);
		}
		
		//将抽取的手牌中的非爆竹牌加入手牌,爆竹牌移出游戏。
		private function delayedFun4(picked:Array):void{
//			for  (var j:int in picked) {
//				bv.removeChild(picked[j]);
//			}
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			
			var pickedFireCrackers:Array=[];
			var left:Array=[];
			for(var i:int in picked){
				if(picked[i].id == 23){
					pickedFireCrackers.push(picked[i]);
				}else{
					left.push(picked[i]);
				}
			}
			bm.sendCardsToPoint(left, s);
//			App.audio.play(Cons.audio.Deal);
			bm.sendCardsToGraveyard(pickedFireCrackers);
			bv.setReport(StringUtils.getColorString("["+(s.view.pname.text==""?s.view.htxt.text:s.view.pname.text)+"]",0xffff00)+"" + 
				"展示了"+StringUtils.getColorString( "["+(t.view.pname.text==""?t.view.htxt.text:t.view.pname.text)+"]",0xffff00)+"的"+picked.length+"张手牌，" +
				"然后，获得了其中的"+ left.length+"张非【爆竹】牌，并弃置了"+pickedFireCrackers.length+"张【爆竹】");
			TweenMax.delayedCall(1,placeBlack,[pickedFireCrackers.length]);
		}
		
		//年在目标玩家面前放置黑情报
		private function placeBlack(NDFC:int=0):void{ //NDFC = number of discarded firecrackers
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			s.view.showTime(10000);
			if(s.uid==GL.id){
				var temp:Array=[];
				for(var i:int in s.hand){
					if(s.hand[i].color>2){
						var c:ACard=new ACard();
						c.setdata(s.hand[i]);
						temp.push(c);
					}
				}
				bv.showCardDialog(temp,"请选择最多"+NDFC+"张卡放置到目标面前，若不放则直接按确定",NDFC,2);
			}else{
				bv.showInfo("请等待年操作");
			}
		}
		
		//目标玩家选择手里的爆竹砸给年
		private function placeFirecrackers():void{ //NDFC = number of discarded firecrackers
			var s:Player = phash.get(tvo.sponsor);
			var t:Player = phash.get(tvo.target);
			var arr:Array=data.firecrackers;
			t.view.showTime(10000);
			if(t.uid==GL.id){
				var temp:Array=[];
				for(var i:int in arr){
					var c:ACard=new ACard();
					c.setdata(arr[i]);
					temp.push(c);
				}
				bv.showCardDialog(temp,"请选择要放置到年面前的情报",temp.length,2);
			}else{
				bv.showInfo("请等待"+(t.view.pname.text==""?t.view.htxt.text:t.view.pname.text)+"操作");
			}
		}
	}
}