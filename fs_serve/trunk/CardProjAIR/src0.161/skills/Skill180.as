package skills
{
	import flash.events.MouseEvent;
	import com.greensock.TweenMax;
	
	import core.mng.Player;
	import core.mng.SFS;
	
	import datas.Cons;
	import datas.GL;
	import datas.SkillVO;
	
	import utils.Rand;
	import views.ACard;

		/**
		 * 横行霸道
		 */	
		public class Skill180 extends Skill
		{
			override public function play(tvo1:SkillVO,obj:Object):void
			{
				super.play(tvo1,obj);
				playNormalAni();
//				App.audio.play(Cons.audio.YELLOW);
				App.audio.play("assets/sounds/skill/"+rid+"/"+id+""+Rand.range(1,4)+".mp3");
			}
		}
}